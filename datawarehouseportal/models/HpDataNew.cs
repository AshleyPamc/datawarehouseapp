﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace datawarehouseportal.models
{
    public class HpDataNew
    {
        public string hpcode{ get; set; }
        public string hpname{ get; set; }
        public string lobCode{ get; set; }
        public string hpLongDesc{ get; set; }
        public string hpType{ get; set; }
        public string opt{ get; set; }
        public string claimcount{ get; set; }
        public string specialtyGrp{ get; set; }
        public string revenueTypeHp{ get; set; }
        public bool filterOnHp{ get; set; }
        public string whereClause{ get; set; }
        public bool isRisk{ get; set; }
        public string svcDateTo{ get; set; }
        public string svcDateFrom{ get; set; }
        public string filterExcludeDt{ get; set; }
        public string hpsID{ get; set; }
        public bool existFactClaim{ get; set; }
    }
}
