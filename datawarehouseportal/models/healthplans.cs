﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace datawarehouseportal.models
{
    public class healthplans
    {
        public string hpname { get; set; }
        public string hpcode { get; set; }
        public string opt { get; set; }
        public int sid { get; set; }
        public bool risk { get; set; }
    }
}
