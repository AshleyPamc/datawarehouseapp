﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace datawarehouseportal.models
{
    public class ProvProfDetail
    {
        public string providerID { get; set; }
        public string AvgDate { get; set; }

        public string firstAction { get; set; }
        public string reviewDate { get; set; }
        public string reviewAction { get; set; }
        public string provclass { get; set; }
        public string ProfileDt { get; set; }
        public string Pos1Billed { get; set; }
        public string Pos2Billed { get; set; }
        public string Pos3Billed { get; set; }
        public string Pos4Billed { get; set; }
        public string Ant1Billed { get; set; }
        public string Ant2Billed { get; set; }
        public string Ant3Billed { get; set; }
        public string Ant4Billed { get; set; }
        public string PosRestBilled { get; set; }
        public string AntRestBilled { get; set; }
        public string RestQtyPerMemb { get; set; }
        public string AuthToMemb { get; set; }
        public string ClaimCostNet { get; set; }
        public string MemberCost { get; set; }
        public string Visitrate { get; set; }
        public string comment { get; set; }

    }
}
