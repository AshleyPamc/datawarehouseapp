﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace datawarehouseportal.models
{
    public class revdata
    {
        public string invoiceDate{ get; set; }
        public string invoicesid{ get; set; }
        public string hpsid { get; set; }
        public string hpname{ get; set; }
        public string bencount { get; set; }
        public string unitprice { get; set; }
        public string incVat { get; set; }
        public string vat { get; set; }
        public string excVat { get; set; }
        public string typeOpt{ get; set; }
        public string amount8110 { get; set; }
        public string amount8109 { get; set; }
        public string adjAmount { get; set; }
        public string adjAmountExecVat { get; set; }
    }
}
