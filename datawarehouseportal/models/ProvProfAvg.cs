﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace datawarehouseportal.models
{
    public class ProvProfAvg
    {
        public string AvgDate { get; set; }
        public string ProfileDt { get; set; }
        public string Pos1Billed { get; set; }
        public string Pos2Billed { get; set; }
        public string Pos3Billed { get; set; }
        public string Pos4Billed { get; set; }
        public string Ant1Billed { get; set; }
        public string Ant2Billed { get; set; }
        public string Ant3Billed { get; set; }
        public string Ant4Billed { get; set; }
        public string PosRestBilled { get; set; }
        public string AntRestBilled { get; set; }
        public string RestQtyPerMemb { get; set; }
        public string AuthToMemb { get; set; }
        public string ClaimCostNet { get; set; }
        public string MemberCost { get; set; }
        public string Visitrate { get; set; }
    }
}
