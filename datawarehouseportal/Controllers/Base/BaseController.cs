﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace datawarehouseportal.Controllers.Base
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContextController : ControllerBase
    {
        public static IHostingEnvironment _env;
        public IConfiguration _configuration;
        public static string _dwConnectionString;
        //public static string _trainingDataConnectionStrting;
        public static bool _allowAdmin;

        internal SqlConnection _dwConnection = null;

        public ContextController(IHostingEnvironment env, IConfiguration configuration)
        {
            _env = env;
            _configuration = configuration;
            var connections = _configuration.GetSection("ConnectionStrings").GetChildren().AsEnumerable();
            var webCon = _configuration.GetSection("WebConnections").GetChildren().AsEnumerable();
           /// var allowedLogin = _configuration.GetSection("AllowAdminLogin").GetChildren().AsEnumerable();
            _dwConnectionString = connections.ToArray()[0].Value;
           // _allowAdmin = Convert.ToBoolean(allowedLogin.ToArray()[0].Value);
            this._dwConnection = new SqlConnection(_dwConnectionString);
        }
        public string DWDatabase
        {
            get
            {
                return this._dwConnection.Database;
            }
        }

    }
}