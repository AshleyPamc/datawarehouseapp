﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using datawarehouseportal.models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace datawarehouseportal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CaptureController : Base.ContextController
    {
        public CaptureController(IHostingEnvironment env, IConfiguration configuration) : base(env, configuration)
        {

        }

        [HttpGet]
        [Route("GetHealthPlans")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<healthplans> GetHealthPlans()
        {
            List<healthplans> hp = new List<healthplans>();
            try
            {

                using (SqlConnection cn = new SqlConnection(_dwConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = $"SELECT * FROM {DWDatabase}.dbo.dimHealthPlan";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow item in dt.Rows)
                    {
                        healthplans h = new healthplans();
                        h.hpcode = item["HPCODE"].ToString();
                        h.hpname = item["HPNAME"].ToString();
                        h.opt = item["OPT"].ToString();
                        h.sid = Convert.ToInt32(item["HEALTHPLANSID"]);
                        if (item["isRisk"].ToString().ToUpper() == "FALSE")
                        {
                            h.risk = false;
                        }
                        else
                        {
                            h.risk = true;
                        }
                        //h.risk = Convert.ToBoolean(item["isRisk"].ToString());

                        hp.Add(h);
                    }


                }

            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            return hp;
        }

        [HttpGet]
        [Route("GetTypes")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<Types> GetTypes()
        {
            List<Types> types = new List<Types>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_dwConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT DISTINCT [Type] FROM {DWDatabase}.dbo.FactRevenue";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow item in dt.Rows)
                    {
                        Types type = new Types();
                        type.type = item["Type"].ToString();
                        types.Add(type);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            return types;
        }

        [HttpGet]
        [Route("GetRevData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<revdata> GetRevData()
        {
            List<revdata> data = new List<revdata>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_dwConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.CommandText = $"SELECT \n" +
                        $"fr.InvoiceDate, \n" +
                        $"fr.INVOICEDATESID, \n" +
                        $"fr.HEALTHPLANSID, \n" +
                        $"fr.BeneficiaryCount, \n" +
                        $"fr.UnitPrice, \n" +
                        $"fr.[Revenue Incl vat], \n" +
                        $"fr.Vat, \n" +
                        $"fr.[Revenue Excl Vat], \n" +
                        $"fr.[8110Amount], \n" +
                        $"fr.[8109Amount], \n" +
                        $"fr.[AdjRevExcludingVat], \n" +
                        $"fr.[AdjRevIncludingVat], \n" +
                        $"fr.Type, \n" +
                        $"hp.HPNAME, \n" +
                        $"hp.OPT \n" +
                        $" FROM {DWDatabase}.dbo.FactRevenue fr \n" +
                        $"LEFT OUTER JOIN {DWDatabase}.dbo.dimHealthPlan hp on hp.HEALTHPLANSID = fr.HEALTHPLANSID" +
                        $"\n order by fr.InvoiceDate desc";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    foreach (DataRow item in dt.Rows)
                    {
                        revdata type = new revdata();
                        type.invoiceDate = item["InvoiceDate"].ToString().Replace("00:00:00", "");
                        type.invoicesid = item["INVOICEDATESID"].ToString();
                        type.hpsid = item["HEALTHPLANSID"].ToString();
                        type.bencount = item["BeneficiaryCount"].ToString();
                        type.unitprice = item["UnitPrice"].ToString();
                        type.incVat = item["Revenue Incl vat"].ToString();
                        type.vat = item["Vat"].ToString();
                        type.excVat = item["Revenue Excl Vat"].ToString();
                        type.typeOpt = item["Type"].ToString();
                        type.hpname = item["HPNAME"].ToString() + " - " + item["OPT"].ToString();
                        if (DBNull.Value.Equals(type.adjAmount))
                        {

                            type.adjAmount = "0";
                        }
                        else
                        {
                            type.adjAmount = item["AdjRevIncludingVat"].ToString();
                        }
                        if (DBNull.Value.Equals(type.adjAmount))
                        {
                            type.adjAmountExecVat = "0";
                        }
                        else
                        {
                            type.adjAmountExecVat = item["AdjRevExcludingVat"].ToString();
                        }
                        if (DBNull.Value.Equals(type.amount8109))
                        {
                            type.amount8109 = "0";

                        }
                        else
                        {
                            type.amount8109 = item["8109Amount"].ToString();
                        }
                        if (DBNull.Value.Equals(type.amount8110))
                        {
                            type.amount8110 = "0";
                        }
                        else
                        {
                            type.amount8110 = item["8110Amount"].ToString();

                        }
                        data.Add(type);
                    }
                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }
            return data;
        }

        [HttpPost]
        [Route("AddNewData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public validation AddNewData([FromBody] revdata data)
        {
            validation vr = new validation();
            try
            {
                using (SqlConnection cn = new SqlConnection(_dwConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", data.invoiceDate));
                    if (data.invoicesid.Contains("/") || (Convert.ToDateTime(data.invoiceDate).ToString("yyyyMMdd") != data.invoicesid))
                    {
                        data.invoicesid = Convert.ToDateTime(data.invoiceDate).ToString("yyyyMMdd");
                        cmd.Parameters.Add(new SqlParameter("@datesid", data.invoicesid));

                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@datesid", data.invoicesid));
                    }

                    cmd.Parameters.Add(new SqlParameter("@hpsid", data.hpsid));
                    cmd.Parameters.Add(new SqlParameter("@bencount", data.bencount));
                    cmd.Parameters.Add(new SqlParameter("@unit", data.unitprice));
                    cmd.Parameters.Add(new SqlParameter("@revvat", data.incVat));
                    cmd.Parameters.Add(new SqlParameter("@vat", data.vat));
                    cmd.Parameters.Add(new SqlParameter("@revexc", data.excVat));
                    cmd.Parameters.Add(new SqlParameter("@type", data.typeOpt));
                    cmd.Parameters.Add(new SqlParameter("@adjAmount", data.adjAmount));
                    cmd.Parameters.Add(new SqlParameter("@adjAmountExecVat", data.adjAmountExecVat));
                    cmd.Parameters.Add(new SqlParameter("@amount8109", data.amount8109));
                    cmd.Parameters.Add(new SqlParameter("@amount8110", data.amount8110));


                    cmd.CommandText = $"INSERT INTO {DWDatabase}.dbo.FactRevenue (InvoiceDate,INVOICEDATESID,HEALTHPLANSID,BeneficiaryCount,UnitPrice" +
                        $",[Revenue Incl vat],Vat,[Revenue Excl Vat],Type,[8110Amount],[8109Amount],AdjRevExcludingVat,AdjRevIncludingVat) " +
                        $"VALUES (@date,@datesid,@hpsid,@bencount,@unit,@revvat,@vat,@revexc,@type,@amount8110,@amount8109,@adjAmountExecVat,@adjAmount)";

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }


            return vr;
        }

        [HttpPost]
        [Route("UpdateData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public validation UpdateData([FromBody] revdata[] data)
        {
            validation vr = new validation();
            try
            {
                using (SqlConnection cn = new SqlConnection(_dwConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", data[1].invoiceDate));
                    if (data[1].invoicesid.Contains("/") || (Convert.ToDateTime(data[1].invoiceDate).ToString("yyyyMMdd") != data[1].invoicesid))
                    {
                        data[1].invoicesid = Convert.ToDateTime(data[1].invoiceDate).ToString("yyyyMMdd");
                        cmd.Parameters.Add(new SqlParameter("@datesid", data[1].invoicesid));

                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@datesid", data[1].invoicesid));
                    }
                    cmd.Parameters.Add(new SqlParameter("@hpsid", data[1].hpsid));
                    cmd.Parameters.Add(new SqlParameter("@bencount", data[1].bencount));
                    cmd.Parameters.Add(new SqlParameter("@unit", data[1].unitprice));
                    cmd.Parameters.Add(new SqlParameter("@revvat", data[1].incVat));
                    cmd.Parameters.Add(new SqlParameter("@vat", data[1].vat));
                    cmd.Parameters.Add(new SqlParameter("@revexc", data[1].excVat));
                    cmd.Parameters.Add(new SqlParameter("@type", data[1].typeOpt));
                    cmd.Parameters.Add(new SqlParameter("@adjAmount", data[1].adjAmount));
                    cmd.Parameters.Add(new SqlParameter("@adjAmountExecVat", data[1].adjAmountExecVat));
                    cmd.Parameters.Add(new SqlParameter("@amount8109", data[1].amount8109));
                    cmd.Parameters.Add(new SqlParameter("@amount8110", data[1].amount8110));
                    cmd.Parameters.Add(new SqlParameter("@olddate", data[0].invoiceDate));
                    if (data[0].invoicesid.Contains("/") || (Convert.ToDateTime(data[0].invoiceDate).ToString("yyyyMMdd") != data[0].invoicesid))
                    {
                        data[0].invoicesid = Convert.ToDateTime(data[0].invoiceDate).ToString("yyyyMMdd");
                        cmd.Parameters.Add(new SqlParameter("@olddatesid", data[0].invoicesid));

                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@olddatesid", data[0].invoicesid));
                    }

                    cmd.Parameters.Add(new SqlParameter("@oldhpsid", data[0].hpsid));
                    cmd.Parameters.Add(new SqlParameter("@oldbencount", data[0].bencount));
                    cmd.Parameters.Add(new SqlParameter("@oldunit", data[0].unitprice));
                    cmd.Parameters.Add(new SqlParameter("@oldrevvat", data[0].incVat));
                    cmd.Parameters.Add(new SqlParameter("@oldvat", data[0].vat));
                    cmd.Parameters.Add(new SqlParameter("@oldrevexc", data[0].excVat));
                    cmd.Parameters.Add(new SqlParameter("@oldtype", data[0].typeOpt));
                    cmd.Parameters.Add(new SqlParameter("@oldadjAmount", data[0].adjAmount));
                    cmd.Parameters.Add(new SqlParameter("@oldadjAmountExecVat", data[0].adjAmountExecVat));
                    cmd.Parameters.Add(new SqlParameter("@oldamount8109", data[0].amount8109));
                    cmd.Parameters.Add(new SqlParameter("@oldamount8110", data[0].amount8110));

                    cmd.CommandText = $"UPDATE {DWDatabase}.dbo.FactRevenue SET InvoiceDate = @date,INVOICEDATESID =@datesid ,HEALTHPLANSID=@hpsid,BeneficiaryCount= @bencount," +
                        $"UnitPrice =@unit,[Revenue Incl vat] =@revvat,Vat =@vat,[Revenue Excl Vat] =@revexc ,Type =@type," +
                        $" [8110Amount] =@amount8110,[8109Amount] =@amount8109,AdjRevExcludingVat= @adjAmountExecVat,AdjRevIncludingVat=@adjAmount \n" +
                        $"WHERE InvoiceDate = @olddate AND INVOICEDATESID =@olddatesid  AND HEALTHPLANSID=@oldhpsid AND BeneficiaryCount= @oldbencount AND " +
                        $"UnitPrice =@oldunit AND [Revenue Incl vat] =@oldrevvat AND Vat =@oldvat AND [Revenue Excl Vat] =@oldrevexc  " +
                        $"AND Type =@oldtype AND AdjRevExcludingVat= @oldadjAmountExecVat AND AdjRevIncludingVat=@oldadjAmount";

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }


            return vr;
        }

        [HttpPost]
        [Route("DeleteData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public validation DeleteData([FromBody] revdata data)
        {
            validation vr = new validation();
            try
            {
                using (SqlConnection cn = new SqlConnection(_dwConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", data.invoiceDate));
                    if (data.invoicesid.Contains("/") || (Convert.ToDateTime(data.invoiceDate).ToString("yyyyMMdd") != data.invoicesid))
                    {
                        data.invoicesid = Convert.ToDateTime(data.invoiceDate).ToString("yyyyMMdd");
                        cmd.Parameters.Add(new SqlParameter("@datesid", data.invoicesid));

                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@datesid", data.invoicesid));
                    }
                    cmd.Parameters.Add(new SqlParameter("@hpsid", data.hpsid));
                    cmd.Parameters.Add(new SqlParameter("@bencount", data.bencount));
                    cmd.Parameters.Add(new SqlParameter("@unit", data.unitprice));
                    cmd.Parameters.Add(new SqlParameter("@revvat", data.incVat));
                    cmd.Parameters.Add(new SqlParameter("@vat", data.vat));
                    cmd.Parameters.Add(new SqlParameter("@revexc", data.excVat));
                    cmd.Parameters.Add(new SqlParameter("@type", data.typeOpt));
                    cmd.Parameters.Add(new SqlParameter("@adjAmount", data.adjAmount));
                    cmd.Parameters.Add(new SqlParameter("@adjAmountExecVat", data.adjAmountExecVat));
                    cmd.Parameters.Add(new SqlParameter("@amount8109", data.amount8109));
                    cmd.Parameters.Add(new SqlParameter("@amount8110", data.amount8110));


                    cmd.CommandText = $"DELETE FROM {DWDatabase}.dbo.FactRevenue \n" +
                        $"WHERE InvoiceDate = @date AND INVOICEDATESID =@datesid  AND HEALTHPLANSID=@hpsid AND BeneficiaryCount= @bencount AND " +
                        $"UnitPrice =@unit AND [Revenue Incl vat] =@revvat AND Vat =@vat AND [Revenue Excl Vat] =@revexc  AND Type =@type" +
                        $" AND [8110Amount] =@amount8110 AND [8109Amount] =@amount8109 AND AdjRevExcludingVat= @adjAmountExecVat AND AdjRevIncludingVat=@adjAmount";

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception e)
            {
                LogError(e.Message, e.StackTrace);
                var msg = e.Message;
            }


            return vr;
        }

        [HttpGet]
        [Route("GetDimHpData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<hpData> GetDimHpData()
        {
            List<hpData> list = new List<hpData>();
            try
            {
                SqlConnection cn = new SqlConnection(_dwConnectionString);
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                DataTable dt = new DataTable();

                cmd.CommandTimeout = 6000;
                cmd.CommandText = $"SELECT [HEALTHPLANSID]" +
                                   ",[HPCODE] " +
                                   ",[HPNAME] " +
                                   ",[OPT] " +
                                   ",[SpecialityGroup]" +
                                   ",[ServiceDateFrom] " +
                                   ",[ServiceDateTo] " +
                                   ",[FilterExcludeToDate] " +
                                   ",[FilterOnHP] " +
                                   ",[RevenueTypeHP] " +
                                   ",[WHERECLAUSE] " +
                                   ",[isRisk] " +
                                   ",[ExistInFactClaims] " +

                                  " FROM[DRC_DW].[dbo].[dimHealthPlan] " +
                                  "ORDER BY [HPCODE] ASC";

                dt.Load(cmd.ExecuteReader());

                foreach (DataRow row in dt.Rows)
                {
                    hpData data = new hpData();
                    data.hpsID = row["HEALTHPLANSID"].ToString();
                    data.hpcode = row["HPCODE"].ToString();
                    data.hpname = row["HPNAME"].ToString();
                    data.opt = row["OPT"].ToString();
                    data.specialtyGrp = row["SpecialityGroup"].ToString();
                    data.svcDateFrom = row["ServiceDateFrom"].ToString();
                    data.svcDateTo = row["ServiceDateTo"].ToString();
                    data.filterExcludeDt = row["FilterExcludeToDate"].ToString();
                    if (DBNull.Value.Equals(row["FilterOnHP"]))
                    {
                        data.filterOnHp = false;
                    }
                    else
                    {
                        data.filterOnHp = Convert.ToBoolean(row["FilterOnHP"]);
                    }
                    if (DBNull.Value.Equals(row["ExistInFactClaims"]))
                    {
                        data.existFactClaim = false;
                    }
                    else
                    {
                        data.existFactClaim = Convert.ToBoolean(row["ExistInFactClaims"]);
                    }
                    data.revenueTypeHp = row["RevenueTypeHP"].ToString();
                    data.whereClause = row["WHERECLAUSE"].ToString();
                    if (DBNull.Value.Equals(row["isRisk"]))
                    {
                        data.isRisk = false;
                    }
                    else
                    {
                        data.isRisk = Convert.ToBoolean(row["isRisk"]);
                    }

                    list.Add(data);


                }

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("UpdateDimHealthplanData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public validation UpdateDimHealthplanData([FromBody] hpData data)
        {
            validation vr = new validation();
            try
            {
                SqlConnection cn = new SqlConnection(_dwConnectionString);
                SqlCommand cmd = new SqlCommand();

                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                cmd.Connection = cn;

                cmd.Parameters.Add(new SqlParameter("@sid", data.hpsID));
                cmd.Parameters.Add(new SqlParameter("@isRisk", data.isRisk));
                cmd.Parameters.Add(new SqlParameter("@specialtyGrp", data.specialtyGrp));
                cmd.Parameters.Add(new SqlParameter("@revenueTypeHp", data.revenueTypeHp));
                cmd.Parameters.Add(new SqlParameter("@filterOnHp", data.filterOnHp));
                cmd.Parameters.Add(new SqlParameter("@whereClause", data.whereClause));
                cmd.Parameters.Add(new SqlParameter("@svcDateTo", data.svcDateTo));
                cmd.Parameters.Add(new SqlParameter("@svcDateFrom", data.svcDateFrom));
                cmd.Parameters.Add(new SqlParameter("@filterExcludeDt", data.filterExcludeDt));
                cmd.Parameters.Add(new SqlParameter("@lastChange", DateTime.Now));
                cmd.Parameters.Add(new SqlParameter("@ExistInFactClaims", data.existFactClaim));

                if (data.whereClause != "" && data.whereClause != null)
                {
                    cmd.CommandText = "UPDATE dimHealthPlan SET " +
                " isRisk = @isrisk, WHERECLAUSE = @whereClause, revenueTypeHP = @revenueTypeHp," +
                " FilterOnHP = @filterOnHp, FilterExcludeToDate = @filterExcludeDt, ServiceDateFrom = @svcDateFrom," +
                " ServiceDateTo = @svcDateTo, SpecialityGroup = @specialtyGrp, ExistInFactClaims = @ExistInFactClaims " +
                " WHERE HEALTHPLANSID = @sid";
                }
                else
                {
                    cmd.CommandText = "UPDATE dimHealthPlan SET " +
                " isRisk = @isrisk, WHERECLAUSE = NULL, revenueTypeHP = @revenueTypeHp," +
                " FilterOnHP = @filterOnHp, FilterExcludeToDate = @filterExcludeDt, ServiceDateFrom = @svcDateFrom," +
                " ServiceDateTo = @svcDateTo, SpecialityGroup = @specialtyGrp, ExistInFactClaims = @ExistInFactClaims " +
                " WHERE HEALTHPLANSID = @sid";
                }

                cmd.ExecuteNonQuery();
                vr.valid = true;

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }

            return vr;
        }

        [HttpPost]
        [Route("NewDimHealthplanData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public validation NewDimHealthplanData([FromBody] HpDataNew data)
        {
            validation vr = new validation();
            try
            {
                SqlConnection cn = new SqlConnection(_dwConnectionString);
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;

                cmd.CommandTimeout = 6000;

                cmd.Parameters.Add(new SqlParameter("@hpcode", data.hpcode));
                cmd.Parameters.Add(new SqlParameter("@hpname", data.hpname));
                cmd.Parameters.Add(new SqlParameter("@lobCode", data.lobCode));
                cmd.Parameters.Add(new SqlParameter("@opt", data.opt));
                if (data.hpLongDesc == null || data.hpLongDesc == "")
                {
                    data.hpLongDesc = " ";
                }
                cmd.Parameters.Add(new SqlParameter("@hpLongDesc", data.hpLongDesc));
                cmd.Parameters.Add(new SqlParameter("@hpType", data.hpType));
                cmd.Parameters.Add(new SqlParameter("@filterOnHp", data.filterOnHp));
                cmd.Parameters.Add(new SqlParameter("@isRisk", data.isRisk));
                cmd.Parameters.Add(new SqlParameter("@filterExcludeDt", data.filterExcludeDt));
                cmd.Parameters.Add(new SqlParameter("@svcDateFrom", data.svcDateFrom));
                cmd.Parameters.Add(new SqlParameter("@svcDateTo", data.svcDateTo));
                if(data.whereClause == null || data.whereClause == "")
                {
                    data.whereClause = " ";
                }
                cmd.Parameters.Add(new SqlParameter("@whereClause", data.whereClause));
                cmd.Parameters.Add(new SqlParameter("@revenueTypeHp", data.revenueTypeHp));
                cmd.Parameters.Add(new SqlParameter("@specialtyGrp", data.specialtyGrp));
                cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now));
                cmd.Parameters.Add(new SqlParameter("@claimCount", data.claimcount));
                cmd.Parameters.Add(new SqlParameter("@existFactClaim", data.existFactClaim));



                if (data.whereClause != "" && data.whereClause != null)
                {
                    cmd.CommandText = "INSERT INTO [dbo].[dimHealthPlan]" +
                                              "([HPCODE],[HPNAME],[LOBCODE],[LONDESC],[HPTYPE],[OPT]," +
                                              "[SpecialityGroup],[INSERTDATE],[LASTUPDATEDATE],[ServiceDateFrom]," +
                                              "[ServiceDateTo],[CliamCount],[ExistInFactClaims],[FilterExcludeToDate]," +
                                              "[FilterOnHP],[RevenueTypeHP],[WHERECLAUSE],[isRisk])" +
                                              " VALUES(@hpcode,@hpname,@lobCode,@hpLongDesc,@hpType,@opt," +
                                              "@specialtyGrp,@date,@date,@svcDateFrom,@svcDateTo" +
                                              ",@claimCount,@existFactClaim,@filterExcludeDt," +
                                              "@filterOnHp,@revenueTypeHp,@whereClause,@isRisk)";
                }
                else
                {
                    cmd.CommandText = "INSERT INTO [dbo].[dimHealthPlan]" +
                                             "([HPCODE],[HPNAME],[LOBCODE],[LONDESC],[HPTYPE],[OPT]," +
                                             "[SpecialityGroup],[INSERTDATE],[LASTUPDATEDATE],[ServiceDateFrom]," +
                                             "[ServiceDateTo],[CliamCount],[ExistInFactClaims],[FilterExcludeToDate]," +
                                             "[FilterOnHP],[RevenueTypeHP],[WHERECLAUSE],[isRisk])" +
                                             " VALUES(@hpcode,@hpname,@lobCode,@hpLongDesc,@hpType,@opt," +
                                             "@specialtyGrp,@date,@date,@svcDateFrom,@svcDateTo" +
                                             ",@claimCount,@existFactClaim,@filterExcludeDt," +
                                             "@filterOnHp,@revenueTypeHp,NULL,@isRisk)";
                }
                cmd.ExecuteNonQuery();
                vr.valid = true;

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }

            return vr;
        }

        [HttpPost]
        [Route("AddNewProvProfAvgData")]
        public validation AddNewProvProfAvgData([FromBody] ProvProfAvg data)
        {
            validation vr = new validation();
            try
            {
                SqlConnection cn = new SqlConnection(_dwConnectionString);
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandTimeout = 6000;

                cmd.Parameters.Add(new SqlParameter("@AVGDATE", data.AvgDate));
                cmd.Parameters.Add(new SqlParameter("@PROFILEDATE", data.ProfileDt));
                cmd.Parameters.Add(new SqlParameter("@POS1BILLED", data.Pos1Billed));
                cmd.Parameters.Add(new SqlParameter("@POS2BILLED", data.Pos2Billed));
                cmd.Parameters.Add(new SqlParameter("@POS3BILLED", data.Pos3Billed));
                cmd.Parameters.Add(new SqlParameter("@POS4BILLED", data.Pos4Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT1BILLED", data.Ant1Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT2BILLED", data.Ant2Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT3BILLED", data.Ant3Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT4BILLED", data.Ant4Billed));
                cmd.Parameters.Add(new SqlParameter("@POSRESTBILLED", data.PosRestBilled));
                cmd.Parameters.Add(new SqlParameter("@ANTRESTBILLED", data.AntRestBilled));
                cmd.Parameters.Add(new SqlParameter("@RESTQTYPERMEMBER", data.RestQtyPerMemb));
                cmd.Parameters.Add(new SqlParameter("@AUTHTOMEMBER", data.AuthToMemb));
                cmd.Parameters.Add(new SqlParameter("@CLAIMCOSTNET", data.ClaimCostNet));
                cmd.Parameters.Add(new SqlParameter("@MEMBERCOST", data.MemberCost));
                cmd.Parameters.Add(new SqlParameter("@VISITRATE", data.Visitrate));



                cmd.CommandText = $"INSERT INTO ProviderProfilingIndustryAvg (AVGDATE,PROFILEDATE,POS1BILLED," +
                    $"POS2BILLED ,POS3BILLED,POS4BILLED,ANT1BILLED ," +
                     "ANT2BILLED ,ANT3BILLED,ANT4BILLED,POSRESTBILLED,ANTRESTBILLED,RESTQTYPERMEMBER ,AUTHTOMEMBER," +
                     "CLAIMCOSTNET,MEMBERCOST,VISITRATE,CREATEDATE,LASTCHANGEDATE) VALUES (@AVGDATE,@PROFILEDATE," +
                     "@POS1BILLED,@POS2BILLED ,@POS3BILLED,@POS4BILLED,@ANT1BILLED ," +
                     "@ANT2BILLED ,@ANT3BILLED,@ANT4BILLED,@POSRESTBILLED,@ANTRESTBILLED,@RESTQTYPERMEMBER ,@AUTHTOMEMBER," +
                     "@CLAIMCOSTNET,@MEMBERCOST,@VISITRATE,GETDATE(),GETDATE())";
                cmd.ExecuteNonQuery();
                vr.valid = true;
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return vr;
        }

        [HttpGet]
        [Route("GetPRoviderProfileAVGData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ProvProfAvg> GetPRoviderProfileAVGData()
        {
            List<ProvProfAvg> list = new List<ProvProfAvg>();
            try
            {
                SqlConnection cn = new SqlConnection(_dwConnectionString);
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandTimeout = 6000;

                DataTable dt = new DataTable();

                cmd.CommandText = "SELECT * FROM ProviderProfilingIndustryAvg";

                dt.Load(cmd.ExecuteReader());

                foreach (DataRow row in dt.Rows)
                {
                    ProvProfAvg p = new ProvProfAvg();

                    p.AvgDate = Convert.ToDateTime(row["AVGDATE"].ToString()).ToString("yyyyMMdd");
                    p.ProfileDt = Convert.ToDateTime(row["PROFILEDATE"].ToString()).ToString("yyyy/MM/dd");
                    p.Pos1Billed = row["POS1BILLED"].ToString();
                    p.Pos1Billed = row["POS1BILLED"].ToString();
                    p.Pos2Billed = row["POS2BILLED"].ToString();
                    p.Pos3Billed = row["POS3BILLED"].ToString();
                    p.Pos4Billed = row["POS4BILLED"].ToString();
                    p.Ant1Billed = row["ANT1BILLED"].ToString();
                    p.Ant2Billed = row["ANT2BILLED"].ToString();
                    p.Ant3Billed = row["ANT3BILLED"].ToString();
                    p.Ant4Billed = row["ANT4BILLED"].ToString();
                    p.PosRestBilled = row["POSRESTBILLED"].ToString();
                    p.AntRestBilled = row["ANTRESTBILLED"].ToString();
                    p.RestQtyPerMemb = row["RESTQTYPERMEMBER"].ToString();
                    p.AuthToMemb = row["AUTHTOMEMBER"].ToString();
                    p.ClaimCostNet = row["CLAIMCOSTNET"].ToString();
                    p.MemberCost = row["MEMBERCOST"].ToString();
                    p.Visitrate = row["VISITRATE"].ToString();
                    list.Add(p);

                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return list;
        }
        [HttpGet]
        [Route("GetPRoviderProfileDetailData")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ProvProfDetail> GetPRoviderProfileDetailData()
        {
            List<ProvProfDetail> list = new List<ProvProfDetail>();
            try
            {
                SqlConnection cn = new SqlConnection(_dwConnectionString);
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandTimeout = 6000;

                DataTable dt = new DataTable();

                cmd.CommandText = "SELECT * FROM ProviderProfiledDetails";

                dt.Load(cmd.ExecuteReader());

                foreach (DataRow row in dt.Rows)
                {
                    ProvProfDetail p = new ProvProfDetail();

                    p.providerID = row["PROVID"].ToString();
                    p.AvgDate = Convert.ToDateTime(row["AVGDATE"].ToString()).ToString("yyyyMMdd");
                    p.ProfileDt = Convert.ToDateTime(row["PROFILEDATE"].ToString()).ToString("yyyy/MM/dd");
                    p.firstAction = row["FIRSTACTION"].ToString();
                    p.reviewDate = Convert.ToDateTime(row["REVIEWDATE"].ToString()).ToString("yyyyMMdd");
                    p.provclass = row["CLASS"].ToString();
                    p.reviewAction = row["REVIEWACTION"].ToString();
                    p.Pos1Billed = row["POS1BILLED"].ToString();
                    p.Pos1Billed = row["POS1BILLED"].ToString();
                    p.Pos2Billed = row["POS2BILLED"].ToString();
                    p.Pos3Billed = row["POS3BILLED"].ToString();
                    p.Pos4Billed = row["POS4BILLED"].ToString();
                    p.Ant1Billed = row["ANT1BILLED"].ToString();
                    p.Ant2Billed = row["ANT2BILLED"].ToString();
                    p.Ant3Billed = row["ANT3BILLED"].ToString();
                    p.Ant4Billed = row["ANT4BILLED"].ToString();
                    p.PosRestBilled = row["POSRESTBILLED"].ToString();
                    p.AntRestBilled = row["ANTRESTBILLED"].ToString();
                    p.RestQtyPerMemb = row["RESTQTYPERMEMBER"].ToString();
                    p.AuthToMemb = row["AUTHTOMEMBER"].ToString();
                    p.ClaimCostNet = row["CLAIMCOSTNET"].ToString();
                    p.MemberCost = row["MEMBERCOST"].ToString();
                    p.Visitrate = row["VISITRATE"].ToString();
                    p.comment = row["COMMENTS"].ToString();
                    list.Add(p);

                }
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return list;
        }

        [HttpPost]
        [Route("DeleteDimHealthplan")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public validation DeleteDimHealthplan([FromBody] hpData data)
        {
            validation vr = new validation();

            try
            {
                SqlConnection cn = new SqlConnection(_dwConnectionString);
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandTimeout = 6000;

                cmd.Parameters.Add(new SqlParameter("@spId", data.hpsID));

                cmd.CommandText = "DELETE FROM dimHealthPlan where HEALTHPLANSID = @spid";
                cmd.ExecuteNonQuery();
                vr.valid = true;
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return vr;


        }


        [HttpPost]
        [Route("UpdateProvProfAvg")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public validation UpdateProvProfAvg([FromBody] List<ProvProfAvg> data)
        {
            validation vr = new validation();
            try
            {
                SqlConnection cn = new SqlConnection(_dwConnectionString);
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandTimeout = 6000;

                cmd.Parameters.Add(new SqlParameter("@AVGDATE", $"{data[1].AvgDate.Substring(0, 4)}-{data[1].AvgDate.Substring(4, 2)}-{data[1].AvgDate.Substring(6, 2)}"));
                cmd.Parameters.Add(new SqlParameter("@PROFILEDATE", data[1].ProfileDt));
                cmd.Parameters.Add(new SqlParameter("@POS1BILLED", data[1].Pos1Billed));
                cmd.Parameters.Add(new SqlParameter("@POS2BILLED", data[1].Pos2Billed));
                cmd.Parameters.Add(new SqlParameter("@POS3BILLED", data[1].Pos3Billed));
                cmd.Parameters.Add(new SqlParameter("@POS4BILLED", data[1].Pos4Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT1BILLED", data[1].Ant1Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT2BILLED", data[1].Ant2Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT3BILLED", data[1].Ant3Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT4BILLED", data[1].Ant4Billed));
                cmd.Parameters.Add(new SqlParameter("@POSRESTBILLED", data[1].PosRestBilled));
                cmd.Parameters.Add(new SqlParameter("@ANTRESTBILLED", data[1].AntRestBilled));
                cmd.Parameters.Add(new SqlParameter("@RESTQTYPERMEMBER", data[1].RestQtyPerMemb));
                cmd.Parameters.Add(new SqlParameter("@AUTHTOMEMBER", data[1].AuthToMemb));
                cmd.Parameters.Add(new SqlParameter("@CLAIMCOSTNET", data[1].ClaimCostNet));
                cmd.Parameters.Add(new SqlParameter("@MEMBERCOST", data[1].MemberCost));
                cmd.Parameters.Add(new SqlParameter("@VISITRATE", data[1].Visitrate));

                cmd.Parameters.Add(new SqlParameter("@AVGDATEORG", $"{data[0].AvgDate.Substring(0, 4)}-{data[0].AvgDate.Substring(4, 2)}-{data[0].AvgDate.Substring(6, 2)}"));
                cmd.Parameters.Add(new SqlParameter("@PROFILEDATEORG", data[0].ProfileDt));
                cmd.Parameters.Add(new SqlParameter("@POS1BILLEDORG", data[0].Pos1Billed));
                cmd.Parameters.Add(new SqlParameter("@POS2BILLEDORG", data[0].Pos2Billed));
                cmd.Parameters.Add(new SqlParameter("@POS3BILLEDORG", data[0].Pos3Billed));
                cmd.Parameters.Add(new SqlParameter("@POS4BILLEDORG", data[0].Pos4Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT1BILLEDORG", data[0].Ant1Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT2BILLEDORG", data[0].Ant2Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT3BILLEDORG", data[0].Ant3Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT4BILLEDORG", data[0].Ant4Billed));
                cmd.Parameters.Add(new SqlParameter("@POSRESTBILLEDORG", data[0].PosRestBilled));
                cmd.Parameters.Add(new SqlParameter("@ANTRESTBILLEDORG", data[0].AntRestBilled));
                cmd.Parameters.Add(new SqlParameter("@RESTQTYPERMEMBERORG", data[0].RestQtyPerMemb));
                cmd.Parameters.Add(new SqlParameter("@AUTHTOMEMBERORG", data[0].AuthToMemb));
                cmd.Parameters.Add(new SqlParameter("@CLAIMCOSTNETORG", data[0].ClaimCostNet));
                cmd.Parameters.Add(new SqlParameter("@MEMBERCOSTORG", data[0].MemberCost));
                cmd.Parameters.Add(new SqlParameter("@VISITRATEORG", data[0].Visitrate));

                cmd.CommandText = $" UPDATE ProviderProfilingIndustryAvg SET AVGDATE = @AVGDATE,PROFILEDATE=@PROFILEDATE, " +
                    $" POS1BILLED= @POS1BILLED," +
                   $" POS2BILLED = @POS2BILLED ,POS3BILLED = @POS3BILLED,POS4BILLED = @POS4BILLED,ANT1BILLED  = @ANT1BILLED, " +
                    " ANT2BILLED = @ANT3BILLED ,ANT3BILLED = @ANT3BILLED,ANT4BILLED = @ANT4BILLED, " +
                    " POSRESTBILLED = @POSRESTBILLED,ANTRESTBILLED = @ANTRESTBILLED,RESTQTYPERMEMBER= @RESTQTYPERMEMBER ,AUTHTOMEMBER = @AUTHTOMEMBER," +
                    " CLAIMCOSTNET =@CLAIMCOSTNET,MEMBERCOST=@MEMBERCOST,VISITRATE=@VISITRATE,LASTCHANGEDATE = GETDATE() " +
                    " WHERE " +
                    " AVGDATE = @AVGDATEORG AND PROFILEDATE=@PROFILEDATEORG AND " +
                    " POS1BILLED= @POS1BILLEDORG AND POS2BILLED=@POS2BILLEDORG  AND POS3BILLED=@POS3BILLEDORG AND " +
                    " POS4BILLED=@POS4BILLEDORG AND ANT1BILLED=@ANT1BILLEDORG  AND " +
                    " ANT2BILLED=@ANT2BILLEDORG  AND ANT3BILLED=@ANT3BILLEDORG AND ANT4BILLED=@ANT4BILLEDORG AND " +
                    " POSRESTBILLED=@POSRESTBILLEDORG AND ANTRESTBILLED=@ANTRESTBILLEDORG AND " +
                    " RESTQTYPERMEMBER=@RESTQTYPERMEMBERORG  AND " +
                    " AUTHTOMEMBER=@AUTHTOMEMBERORG AND " +
                    " CLAIMCOSTNET=@CLAIMCOSTNETORG AND MEMBERCOST=@MEMBERCOSTORG AND VISITRATE=@VISITRATEORG ";
                cmd.ExecuteNonQuery();
                vr.valid = true;
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return vr;
        }

        [HttpPost]
        [Route("DeleteProvProfAvg")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public validation DeleteProvProfAvg([FromBody] ProvProfAvg data)
        {
            validation vr = new validation();
            try
            {
                SqlConnection cn = new SqlConnection(_dwConnectionString);
                if (cn.State != ConnectionState.Open)
                {
                    cn.Open();
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandTimeout = 6000;

                cmd.Parameters.Add(new SqlParameter("@AVGDATEORG", $"{data.AvgDate.Substring(0, 4)}-{data.AvgDate.Substring(4, 2)}-{data.AvgDate.Substring(6, 2)}"));
                cmd.Parameters.Add(new SqlParameter("@PROFILEDATEORG", data.ProfileDt));
                cmd.Parameters.Add(new SqlParameter("@POS1BILLEDORG", data.Pos1Billed));
                cmd.Parameters.Add(new SqlParameter("@POS2BILLEDORG", data.Pos2Billed));
                cmd.Parameters.Add(new SqlParameter("@POS3BILLEDORG", data.Pos3Billed));
                cmd.Parameters.Add(new SqlParameter("@POS4BILLEDORG", data.Pos4Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT1BILLEDORG", data.Ant1Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT2BILLEDORG", data.Ant2Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT3BILLEDORG", data.Ant3Billed));
                cmd.Parameters.Add(new SqlParameter("@ANT4BILLEDORG", data.Ant4Billed));
                cmd.Parameters.Add(new SqlParameter("@POSRESTBILLEDORG", data.PosRestBilled));
                cmd.Parameters.Add(new SqlParameter("@ANTRESTBILLEDORG", data.AntRestBilled));
                cmd.Parameters.Add(new SqlParameter("@RESTQTYPERMEMBERORG", data.RestQtyPerMemb));
                cmd.Parameters.Add(new SqlParameter("@AUTHTOMEMBERORG", data.AuthToMemb));
                cmd.Parameters.Add(new SqlParameter("@CLAIMCOSTNETORG", data.ClaimCostNet));
                cmd.Parameters.Add(new SqlParameter("@MEMBERCOSTORG", data.MemberCost));
                cmd.Parameters.Add(new SqlParameter("@VISITRATEORG", data.Visitrate));

                cmd.CommandText = $" DELETE FROM ProviderProfilingIndustryAvg " +
    " WHERE " +
    " AVGDATE = @AVGDATEORG AND PROFILEDATE=@PROFILEDATEORG AND " +
    " POS1BILLED= @POS1BILLEDORG AND POS2BILLED=@POS2BILLEDORG  AND POS3BILLED=@POS3BILLEDORG AND " +
    " POS4BILLED=@POS4BILLEDORG AND ANT1BILLED=@ANT1BILLEDORG  AND " +
    " ANT2BILLED=@ANT2BILLEDORG  AND ANT3BILLED=@ANT3BILLEDORG AND ANT4BILLED=@ANT4BILLEDORG AND " +
    " POSRESTBILLED=@POSRESTBILLEDORG AND ANTRESTBILLED=@ANTRESTBILLEDORG AND " +
    " RESTQTYPERMEMBER=@RESTQTYPERMEMBERORG  AND " +
    " AUTHTOMEMBER=@AUTHTOMEMBERORG AND " +
    " CLAIMCOSTNET=@CLAIMCOSTNETORG AND MEMBERCOST=@MEMBERCOSTORG AND VISITRATE=@VISITRATEORG ";
                cmd.ExecuteNonQuery();
                vr.valid = true;
            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
            }
            return vr;
        }


        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "SECURITY";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\CAPTURE\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception e)
            {


            }


        }


    }
}