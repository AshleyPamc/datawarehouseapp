﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace datawarehouseportal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Base.ContextController
    {
        private string filename = "app.txt";

        public AuthController(IHostingEnvironment env, IConfiguration configuration): base(env, configuration)
        {

        }

        [HttpGet]
        [Route("Gettoken")]
        public models.validation Gettoken()
        {
            models.validation vr = new models.validation();
            try
            {
                var token = new JwtSecurityToken();
                var root = _env.WebRootPath;
                var roles = new List<Claim>();

                DirectoryInfo di = new DirectoryInfo($"{root}");
                FileInfo fi = new FileInfo($"{root}\\{filename}");

                string key;

                using(StreamReader reader = new StreamReader(fi.FullName))
                {
                    key = reader.ReadLine();
                    reader.Close();
                }

                var symetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
                var signingCedentials = new SigningCredentials(symetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

                roles.Add(new Claim(ClaimTypes.Role, "NormalUser"));
                token = new JwtSecurityToken(
                    issuer: "pamcdw.in",
                    audience: "authorizedUsers",
                    expires: DateTime.Now.AddHours(8),
                    signingCredentials: signingCedentials,
                    claims: roles);


            }
            catch (Exception e)
            {

                var msg = e.Message;
            }

            return vr;
        }
    }
}