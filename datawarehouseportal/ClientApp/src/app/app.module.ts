import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ClarityModule } from '@clr/angular'
import { AppComponent } from './app.component';
import { CaptureComponent } from './components/capture/capture.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeENZA from '@angular/common/locales/en-ZA';
import { DimHealthPlanComponent } from './components/dim-health-plan/dim-health-plan.component';
import { ProviderProfilingIndAvgComponent } from './components/providerprofilingindavg/providerprofilingindavg.component';
import { ProviderProfiledDetailsComponent } from './components/providerprofileddetails/providerprofileddetails.component';
import { NgxDropzoneModule } from 'ngx-dropzone';

registerLocaleData(localeENZA);

@NgModule({
  declarations: [
    AppComponent,
    CaptureComponent,
    DimHealthPlanComponent,
    ProviderProfilingIndAvgComponent,
    ProviderProfiledDetailsComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    NgxDropzoneModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      { path: 'revenue-capture-data', component: CaptureComponent },
      { path: 'healthplan-capture-data', component: DimHealthPlanComponent },
      { path: 'Provider-Profiling-Industry-Avg', component: ProviderProfilingIndAvgComponent },
      { path: 'Provider-Profiling-Details', component: ProviderProfiledDetailsComponent }

    ])
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-za' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
