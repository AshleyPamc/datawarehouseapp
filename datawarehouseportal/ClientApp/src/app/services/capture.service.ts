import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Healthplans } from '../models/healthplans';
import { hpdata } from '../models/hpdata';
import { Types } from '../models/types';
import { Revdata } from '../models/revdata';
import { Validation } from '../models/validation';
import { ProvProfAvg } from '../models/provprofavg';
import { ProvProfDetail } from '../models/ProvProfDetail';

@Injectable({
  providedIn: 'root'
})
export class CaptureService {

  private _dimHpData: hpdata[] = [];
  private _healthplans: Healthplans[] = [];
  public _hp: Healthplans[] = [];
  public _options: Healthplans[] = [];
  public _optPerHpCode = {};
  public _busy: boolean = false;
  public _types: Types[] = [];
  public _data: Revdata[] = [];
  public _newRevData: Revdata;
  public _deleteRevData: Revdata;
  public _updateRevData: Revdata[] = [];
  public _add: boolean = false;
  public revenueModal: boolean = false;
  public _update: boolean = false;
  public _delete: boolean = false;
  public _filterDate: boolean = false;
  public _hpOpt: boolean = false;
  public _type: boolean = false;
  public providerProfileAvg: ProvProfAvg[] = [];
  public providerProfileDetail: ProvProfDetail[] = [];

  constructor(private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string) {
    this.GetHealthPlans();
  }

  public get hpplans(): Healthplans[] {
    return this._healthplans;
  }
  public set hpplans(value: Healthplans[]) {
    this._healthplans = value;
  }
  public get dimHpData(): hpdata[] {
    return this._dimHpData;
  }
  public set dimHpData(value: hpdata[]) {
    this._dimHpData = value;
  }

  GetHealthPlans() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Capture/GetHealthPlans', {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Healthplans[]) => {
      this._healthplans = [];
      results.forEach((el) => {
        this._healthplans.push(el);
      });
      console.log(this._healthplans);
    },
      (error) => { console.log(error) },
      () => {
        this.GetUniqueHps();
        this.GetTypes();
        this._busy = false;
      });
  }

  GetPRoviderProfileAVGData() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Capture/GetPRoviderProfileAVGData', {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: ProvProfAvg[]) => {
      this.providerProfileAvg = [];
      results.forEach((el) => {
        this.providerProfileAvg.push(el);
      });
      console.log(this.providerProfileAvg);
    },
      (error) => { console.log(error) },
      () => {
        this._busy = false;
      });
  }


  GetUniqueHps() {
    let temp = '';
    this._hp = this.hpplans.filter((a) => {
      if (temp == '') {
        temp = a.hpname;
        return a;
      }
      if (a.hpname == temp) {

      } else {
        temp = a.hpname;
        return a
      }
    });
  }

  GetTypes() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Capture/GetTypes', {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Types[]) => {
      this._types = [];
      results.forEach((el) => {
        this._types.push(el);
      });
    },
      (error) => { console.log(error); },
      () => {
        this._busy = false;
        this.GetRevData();
      })
  }

  GetRevData() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Capture/GetRevData', {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Revdata[]) => {
      this._data = [];
      results.forEach((el) => {
        this._data.push(el);
      });
    },
      (error) => { console.log(error); },
      () => {
        this._busy = false;
        this.GetDimHpData();
      });

  }

  GetDimHpData() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Capture/GetDimHpData', {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: hpdata[]) => {
      this._dimHpData = [];
      results.forEach((el) => {
        this._dimHpData.push(el);
      });
    },
      (error) => { console.log(error); },
      () => {
        this._busy = false;
      });
  }

  GetPRoviderProfileDetailData() {
    this._busy = true;
    this._http.get(this._baseUrl + 'api/Capture/GetPRoviderProfileDetailData', {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: ProvProfDetail[]) => {
      this.providerProfileDetail = [];
      results.forEach((el) => {
        this.providerProfileDetail.push(el);
      });
    },
      (error) => { console.log(error); },
      () => {
        this._busy = false;
      });
  }

  UpdateDimHealthplanData(updatedData: hpdata)
  {
    this._busy = true;

    this._http.post(this._baseUrl + 'api/Capture/UpdateDimHealthplanData',
      updatedData
    ).subscribe(
      (result: Validation) => {

      },
      (error) => { },
      () => {
        this._busy = false;
        this.GetDimHpData();
      }
    );
  }

  NewDimHealthplanData(updatedData: hpdata) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Capture/NewDimHealthplanData',
      updatedData
    ).subscribe(
      (result: Validation) => {

      },
      (error) => { },
      () => {
        this._busy = false;
        this.GetDimHpData();
      }
    );
  }
  NewProvProfAvg(updatedData: ProvProfAvg) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Capture/AddNewProvProfAvgData',
      updatedData
    ).subscribe(
      (result: Validation) => {

      },
      (error) => { },
      () => {
        this._busy = false;
        this.GetPRoviderProfileAVGData();
      }
    );
  }


  UpdateProvProfAvg(updatedData: ProvProfAvg[]) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Capture/UpdateProvProfAvg',
      updatedData
    ).subscribe(
      (result: Validation) => {

      },
      (error) => { },
      () => {
        this._busy = false;
        this.GetPRoviderProfileAVGData();
      }
    );
  }
  DeleteDimHealthplan(updatedData: hpdata) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Capture/DeleteDimHealthplan',
      updatedData
    ).subscribe(
      (result: Validation) => {

      },
      (error) => { },
      () => {
        this._busy = false;
        this.GetDimHpData();
      }
    );
  }

  DeleteProvProfAvg(updatedData: ProvProfAvg) {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Capture/DeleteProvProfAvg',
      updatedData
    ).subscribe(
      (result: Validation) => { 

      },
      (error) => { },
      () => {
        this._busy = false;
        this.GetPRoviderProfileAVGData();
      }
    );
  }

  AddNewData() {
    this._busy = true;
    this._add = false;
    this.revenueModal = false;
    console.log(this._newRevData);
    this._http.post(this._baseUrl + 'api/Capture/AddNewData', this._newRevData, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results) => {

    },
      (error) => { console.log(error) },
      () => {
        this._busy = true;
        this.GetRevData();
      });
  }

  UpdateData() {
    this._updateRevData.push(this._newRevData);
    this._busy = true;
    this._update = false;
    this.revenueModal = false;
    let oldDtSid = this._updateRevData[0].invoicesid;
    if (oldDtSid != this._updateRevData[1].invoicesid) {
      this._updateRevData[1].invoicesid = this._updateRevData[0].invoicesid;
    }
    console.log(this._updateRevData);
    this._http.post(this._baseUrl + 'api/Capture/UpdateData', this._updateRevData, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results) => {

    },
      (error) => { console.log(error) },
      () => {
        this._busy = true;
        this.GetRevData();
      });
  }

  DeleteData() {
    this._updateRevData.push(this._deleteRevData);
    this._busy = true;
    this._update = false;
    this.revenueModal = false;
    this._delete = false;
    this._http.post(this._baseUrl + 'api/Capture/DeleteData', this._deleteRevData, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results) => {

    },
      (error) => { console.log(error) },
      () => {
        this._busy = true;
        this.GetRevData();
      });
  }

}
