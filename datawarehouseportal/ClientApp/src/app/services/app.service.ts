import { Injectable } from '@angular/core';
import { CaptureService } from './capture.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private _captureService: CaptureService) { }

  get CaptureService(): CaptureService
  {
    return this._captureService;
  }
}
