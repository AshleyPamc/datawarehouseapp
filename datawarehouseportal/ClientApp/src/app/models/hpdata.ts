export class hpdata {
  public hpcode: string;
  public  hpname: string;
  public  opt: string;
  public  specialtyGrp: string;
  public  revenueTypeHp: string;
  public  filterOnHp: boolean;
  public  whereClause: string;
  public  isRisk: boolean;
  public  svcDateTo: string;
  public  svcDateFrom: string;
  public filterExcludeDt: string;
  public hpsID: string;
  public existFactClaim: boolean;

}
