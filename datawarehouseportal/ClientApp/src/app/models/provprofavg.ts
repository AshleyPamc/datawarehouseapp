export class ProvProfAvg {

  public avgDate:string;
  public profileDt:string;
  public pos1Billed:string;
  public pos2Billed:string;
  public pos3Billed:string;
  public pos4Billed:string;
  public ant1Billed:string;
  public ant2Billed:string;
  public ant3Billed:string;
  public ant4Billed:string;
  public posRestBilled:string;
  public antRestBilled:string;
  public restQtyPerMemb:string;
  public authToMemb:string;
  public claimCostNet:string;
  public memberCost:string;
  public visitrate: string;

}
