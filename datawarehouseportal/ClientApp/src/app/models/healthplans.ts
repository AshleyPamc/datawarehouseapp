export class Healthplans {

  public hpname: string;
  public hpcode: string;
  public opt: string;
  public sid: number;
  public risk: boolean;

}
