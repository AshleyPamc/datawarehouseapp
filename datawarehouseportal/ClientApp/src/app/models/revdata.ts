export class Revdata {
  public invoiceDate: string;
  public hpname: string;
  public invoicesid: string;
  public hpsid: string;
  public bencount: string;
  public unitprice: string;
  public incVat: string;
  public vat: string;
  public excVat: string;
  public typeOpt: string;
  public amount8110: string;
  public amount8109: string;
  public adjAmount: string;
  public adjAmountExecVat: string;

}
