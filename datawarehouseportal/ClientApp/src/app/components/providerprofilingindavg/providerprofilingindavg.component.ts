import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ProvProfAvg } from '../../models/provprofavg';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-providerprofilingindavg',
  templateUrl: './providerprofilingindavg.component.html',
  styleUrls: ['./providerprofilingindavg.component.css']
})
export class ProviderProfilingIndAvgComponent implements OnInit {


  public showNew: boolean = false;
  public selectedItem: ProvProfAvg = new ProvProfAvg();
  public addNew: boolean = false;
  public _errMsg: ValidationErrors;
  public _errDate: any;
  public showEdit: boolean = false;
  public showDelete: boolean = false;
  public updateWarning: boolean = false;

  @Input() frmProvProfAvgNew: FormGroup;
  @Input() frmProvProfAvgEdit: FormGroup;

  constructor(public _appService: AppService, private fb: FormBuilder, private fbEdit: FormBuilder) {

    this._errMsg = {
      required: 'This field is required!',
      maxLength: 'Field Length Exceeded!',
      minLength: 'Field Length To Short!',
      familyNumber: 'Please complete a family number field first!',
      valid: 'valid',
      invalid: 'Invalid Format!'
    };

    this._appService.CaptureService.GetPRoviderProfileAVGData();
  }

  ngOnInit() {

    this.frmProvProfAvgNew = this.fb.group({

      'AvgDate': [],
      'ProfileDt': ['', Validators.required],
      'Pos1Billed': [],
      'Pos2Billed': [],
      'Pos3Billed': [],
      'Pos4Billed': [],
      'Ant1Billed': [],
      'Ant2Billed': [],
      'Ant3Billed': [],
      'Ant4Billed': [],
      'PosRestBilled': [],
      'AntRestBilled': [],
      'RestQtyPerMemb': [],
      'AuthToMemb': [],
      'ClaimCostNet': [],
      'MemberCost': [],
      'Visitrate': []

    });
    this.frmProvProfAvgEdit = this.fbEdit.group({

      'AvgDate': [],
      'ProfileDt': ['', Validators.required],
      'Pos1Billed': [],
      'Pos2Billed': [],
      'Pos3Billed': [],
      'Pos4Billed': [],
      'Ant1Billed': [],
      'Ant2Billed': [],
      'Ant3Billed': [],
      'Ant4Billed': [],
      'PosRestBilled': [],
      'AntRestBilled': [],
      'RestQtyPerMemb': [],
      'AuthToMemb': [],
      'ClaimCostNet': [],
      'MemberCost': [],
      'Visitrate': []

    });
  }


  //OpenModelNew() {
  //  this.showNew = true;
  //  this.frmProvProfAvgNew.reset()

  //}

  //ValidateDate() {
  //  let c: string = this.frmProvProfAvgNew.get('ProfileDt').value;

  //  if (c == "" || c == null || c == undefined) {
  //    this._errDate = this._errMsg['required'];
  //    this.frmProvProfAvgNew.controls['ProfileDt'].setErrors(this._errMsg['required']);
  //    this.frmProvProfAvgNew.controls['ProfileDt'].setValue('');
  //  } else {
  //    let e: string = c.replace("/", "");
  //     e= e.replace("/", "");

  //    this.frmProvProfAvgNew.controls['AvgDate'].setValue(e);
  //  }


  //}

  //ShowNewWarning() {
  //  this.addNew = true;
  //}

  //CloseModelNEw() {

  //  this.showNew = false;
  //  this.selectedItem = new ProvProfAvg();
  //  this.addNew = false;
  //  this.frmProvProfAvgNew.reset();
  //}

  //AddNewData() {
  //  let c: ProvProfAvg = new ProvProfAvg();

  //  c.avgDate = this.frmProvProfAvgNew.get('AvgDate').value.toString();
  //  c.profileDt = this.frmProvProfAvgNew.get('ProfileDt').value.toString();
  //  c.pos1Billed = this.frmProvProfAvgNew.get('Pos1Billed').value
  //  if (c.pos1Billed == "" || c.pos1Billed == null || c.pos1Billed == undefined) {
  //    c.pos1Billed = "0";
  //  } else {
  //    c.pos1Billed = this.frmProvProfAvgNew.get('Pos1Billed').value.toString();
  //  }
  //  c.pos2Billed = this.frmProvProfAvgNew.get('Pos2Billed').value
  //  if (c.pos2Billed == "" || c.pos2Billed == null || c.pos2Billed == undefined) {
  //    c.pos2Billed = "0";
  //  } else {
  //    c.pos2Billed = this.frmProvProfAvgNew.get('Pos2Billed').value.toString();
  //  }
  //  c.pos3Billed = this.frmProvProfAvgNew.get('Pos3Billed').value
  //  if (c.pos3Billed == "" || c.pos3Billed == null || c.pos3Billed == undefined) {
  //    c.pos3Billed = "0";
  //  } else {
  //    c.pos3Billed = this.frmProvProfAvgNew.get('Pos3Billed').value.toString();
  //  }
  //  c.pos4Billed = this.frmProvProfAvgNew.get('Pos4Billed').value
  //  if (c.pos4Billed == "" || c.pos4Billed == null || c.pos4Billed == undefined) {
  //    c.pos4Billed = "0";
  //  } else {
  //    c.pos4Billed = this.frmProvProfAvgNew.get('Pos4Billed').value.toString();
  //  }
  //  c.ant1Billed = this.frmProvProfAvgNew.get('Ant1Billed').value
  //  if (c.ant1Billed == "" || c.ant1Billed == null || c.ant1Billed == undefined) {
  //    c.ant1Billed = "0";
  //  } else {
  //    c.ant1Billed = this.frmProvProfAvgNew.get('Ant1Billed').value.toString();
  //  }
  //  c.ant2Billed = this.frmProvProfAvgNew.get('Ant2Billed').value
  //  if (c.ant2Billed == "" || c.ant2Billed == null || c.ant2Billed == undefined) {
  //    c.ant2Billed = "0";
  //  } else {
  //    c.ant2Billed = this.frmProvProfAvgNew.get('Ant2Billed').value.toString();
  //  }
  //  c.ant3Billed = this.frmProvProfAvgNew.get('Ant3Billed').value
  //  if (c.ant3Billed == "" || c.ant3Billed == null || c.ant3Billed == undefined) {
  //    c.ant3Billed = "0";
  //  } else {
  //    c.ant3Billed = this.frmProvProfAvgNew.get('Ant3Billed').value.toString();
  //  }
  //  c.ant4Billed = this.frmProvProfAvgNew.get('Ant4Billed').value
  //  if (c.ant4Billed == "" || c.ant4Billed == null || c.ant4Billed == undefined) {
  //    c.ant4Billed = "0";
  //  } else {
  //    c.ant4Billed = this.frmProvProfAvgNew.get('Ant4Billed').value.toString();
  //  }
  //  c.posRestBilled = this.frmProvProfAvgNew.get('PosRestBilled').value
  //  if (c.posRestBilled == "" || c.posRestBilled == null || c.posRestBilled == undefined) {
  //    c.posRestBilled = "0";
  //  } else {
  //    c.posRestBilled = this.frmProvProfAvgNew.get('PosRestBilled').value.toString();
  //  }
  //  c.antRestBilled = this.frmProvProfAvgNew.get('AntRestBilled').value
  //  if (c.antRestBilled == "" || c.antRestBilled == null || c.antRestBilled == undefined) {
  //    c.antRestBilled = "0";
  //  } else {
  //    c.antRestBilled = this.frmProvProfAvgNew.get('AntRestBilled').value.toString();
  //  }
  //  c.restQtyPerMemb = this.frmProvProfAvgNew.get('RestQtyPerMemb').value
  //  if (c.restQtyPerMemb == "" || c.restQtyPerMemb == null || c.restQtyPerMemb == undefined) {
  //    c.restQtyPerMemb = "0";
  //  } else {
  //    c.restQtyPerMemb = this.frmProvProfAvgNew.get('RestQtyPerMemb').value.toString();
  //  }
  //  c.authToMemb = this.frmProvProfAvgNew.get('AuthToMemb').value
  //  if (c.authToMemb == "" || c.authToMemb == null || c.authToMemb == undefined) {
  //    c.authToMemb = "0";
  //  } else {
  //    c.authToMemb = this.frmProvProfAvgNew.get('AuthToMemb').value.toString();
  //  }
  //  c.claimCostNet = this.frmProvProfAvgNew.get('ClaimCostNet').value
  //  if (c.claimCostNet == "" || c.claimCostNet == null || c.claimCostNet == undefined) {
  //    c.claimCostNet = "0";
  //  } else {
  //    c.claimCostNet = this.frmProvProfAvgNew.get('ClaimCostNet').value.toString();
  //  }
  //  c.memberCost = this.frmProvProfAvgNew.get('MemberCost').value
  //  if (c.memberCost == "" || c.memberCost == null || c.memberCost == undefined) {
  //    c.memberCost = "0";
  //  } else {
  //    c.memberCost = this.frmProvProfAvgNew.get('MemberCost').value.toString();
  //  }
  //  c.visitrate = this.frmProvProfAvgNew.get('Visitrate').value
  //  if (c.visitrate == "" || c.visitrate == null || c.visitrate == undefined) {
  //    c.visitrate = "0";
  //  } else {
  //    c.visitrate = this.frmProvProfAvgNew.get('Visitrate').value.toString();
  //  }
  //  this._appService.CaptureService.NewProvProfAvg(c);
  //  this.frmProvProfAvgNew.reset();
  //  this.showNew = false;
  //  this.addNew = false;

  //}

  //EditData(selected: ProvProfAvg) {
  //  this.selectedItem = new ProvProfAvg();
  //  this.selectedItem = selected;

  //  this.frmProvProfAvgEdit.controls['AvgDate'].setValue(this.selectedItem.avgDate);
  //  this.frmProvProfAvgEdit.controls['ProfileDt'].setValue(this.selectedItem.profileDt);
  //  this.frmProvProfAvgEdit.controls['Pos1Billed'].setValue(parseFloat(this.selectedItem.pos1Billed));
  //  this.frmProvProfAvgEdit.controls['Pos2Billed'].setValue(parseFloat(this.selectedItem.pos2Billed));
  //  this.frmProvProfAvgEdit.controls['Pos3Billed'].setValue(parseFloat(this.selectedItem.pos3Billed));
  //  this.frmProvProfAvgEdit.controls['Pos4Billed'].setValue(parseFloat(this.selectedItem.pos4Billed));
  //  this.frmProvProfAvgEdit.controls['Ant1Billed'].setValue(parseFloat(this.selectedItem.ant1Billed));
  //  this.frmProvProfAvgEdit.controls['Ant2Billed'].setValue(parseFloat(this.selectedItem.ant2Billed));
  //  this.frmProvProfAvgEdit.controls['Ant3Billed'].setValue(parseFloat(this.selectedItem.ant3Billed));
  //  this.frmProvProfAvgEdit.controls['Ant4Billed'].setValue(parseFloat(this.selectedItem.ant4Billed));
  //  this.frmProvProfAvgEdit.controls['PosRestBilled'].setValue(parseFloat(this.selectedItem.posRestBilled));
  //  this.frmProvProfAvgEdit.controls['AntRestBilled'].setValue(parseFloat(this.selectedItem.antRestBilled));
  //  this.frmProvProfAvgEdit.controls['RestQtyPerMemb'].setValue(parseFloat(this.selectedItem.restQtyPerMemb));
  //  this.frmProvProfAvgEdit.controls['AuthToMemb'].setValue(parseFloat(this.selectedItem.authToMemb));
  //  this.frmProvProfAvgEdit.controls['ClaimCostNet'].setValue(parseFloat(this.selectedItem.claimCostNet));
  //  this.frmProvProfAvgEdit.controls['MemberCost'].setValue(parseFloat(this.selectedItem.memberCost));
  //  this.frmProvProfAvgEdit.controls['Visitrate'].setValue(parseFloat(this.selectedItem.visitrate));
  //  this.showEdit = true;



  //}

  //ShowUpdateWarning() {
  //  this.updateWarning = true;
  //}

  //ShowDeleteData(selected: ProvProfAvg) {
  //  this.selectedItem = new ProvProfAvg();

  //  this.selectedItem = selected;
  //  this.showDelete = true;

  //}

  //DeleteData() {
  //  this.showDelete = false;
  //  this._appService.CaptureService.DeleteProvProfAvg(this.selectedItem);

  //}

  //CloseModel() {
  //  this.showEdit = false;
  //  this.selectedItem = new ProvProfAvg();
  //}

  //UpdateData() {
  //  let originalData: ProvProfAvg = new ProvProfAvg();
  //  originalData = this.selectedItem;
  //  this.selectedItem = new ProvProfAvg();
  //  let c: ProvProfAvg[] = []

  //  c.push(originalData);

  //  this.selectedItem.avgDate = this.frmProvProfAvgEdit.get('AvgDate').value.toString();
  //  this.selectedItem.profileDt = this.frmProvProfAvgEdit.get('ProfileDt').value.toString();
  //  this.selectedItem.pos1Billed = this.frmProvProfAvgEdit.get('Pos1Billed').value
  //  if (this.selectedItem.pos1Billed == "" || this.selectedItem.pos1Billed == null || this.selectedItem.pos1Billed == undefined) {
  //    this.selectedItem.pos1Billed = "0";
  //  } else {
  //    this.selectedItem.pos1Billed = this.frmProvProfAvgEdit.get('Pos1Billed').value.toString();
  //  }
  //  this.selectedItem.pos2Billed = this.frmProvProfAvgEdit.get('Pos2Billed').value
  //  if (this.selectedItem.pos2Billed == "" || this.selectedItem.pos2Billed == null || this.selectedItem.pos2Billed == undefined) {
  //    this.selectedItem.pos2Billed = "0";
  //  } else {
  //    this.selectedItem.pos2Billed = this.frmProvProfAvgEdit.get('Pos2Billed').value.toString();
  //  }
  //  this.selectedItem.pos3Billed = this.frmProvProfAvgEdit.get('Pos3Billed').value
  //  if (this.selectedItem.pos3Billed == "" || this.selectedItem.pos3Billed == null || this.selectedItem.pos3Billed == undefined) {
  //    this.selectedItem.pos3Billed = "0";
  //  } else {
  //    this.selectedItem.pos3Billed = this.frmProvProfAvgEdit.get('Pos3Billed').value.toString();
  //  }
  //  this.selectedItem.pos4Billed = this.frmProvProfAvgEdit.get('Pos4Billed').value
  //  if (this.selectedItem.pos4Billed == "" || this.selectedItem.pos4Billed == null || this.selectedItem.pos4Billed == undefined) {
  //    this.selectedItem.pos4Billed = "0";
  //  } else {
  //    this.selectedItem.pos4Billed = this.frmProvProfAvgEdit.get('Pos4Billed').value.toString();
  //  }
  //  this.selectedItem.ant1Billed = this.frmProvProfAvgEdit.get('Ant1Billed').value
  //  if (this.selectedItem.ant1Billed == "" || this.selectedItem.ant1Billed == null || this.selectedItem.ant1Billed == undefined) {
  //    this.selectedItem.ant1Billed = "0";
  //  } else {
  //    this.selectedItem.ant1Billed = this.frmProvProfAvgEdit.get('Ant1Billed').value.toString();
  //  }
  //  this.selectedItem.ant2Billed = this.frmProvProfAvgEdit.get('Ant2Billed').value
  //  if (this.selectedItem.ant2Billed == "" || this.selectedItem.ant2Billed == null || this.selectedItem.ant2Billed == undefined) {
  //    this.selectedItem.ant2Billed = "0";
  //  } else {
  //    this.selectedItem.ant2Billed = this.frmProvProfAvgEdit.get('Ant2Billed').value.toString();
  //  }
  //  this.selectedItem.ant3Billed = this.frmProvProfAvgEdit.get('Ant3Billed').value
  //  if (this.selectedItem.ant3Billed == "" || this.selectedItem.ant3Billed == null || this.selectedItem.ant3Billed == undefined) {
  //    this.selectedItem.ant3Billed = "0";
  //  } else {
  //    this.selectedItem.ant3Billed = this.frmProvProfAvgEdit.get('Ant3Billed').value.toString();
  //  }
  //  this.selectedItem.ant4Billed = this.frmProvProfAvgEdit.get('Ant4Billed').value
  //  if (this.selectedItem.ant4Billed == "" || this.selectedItem.ant4Billed == null || this.selectedItem.ant4Billed == undefined) {
  //    this.selectedItem.ant4Billed = "0";
  //  } else {
  //    this.selectedItem.ant4Billed = this.frmProvProfAvgEdit.get('Ant4Billed').value.toString();
  //  }
  //  this.selectedItem.posRestBilled = this.frmProvProfAvgEdit.get('PosRestBilled').value
  //  if (this.selectedItem.posRestBilled == "" || this.selectedItem.posRestBilled == null || this.selectedItem.posRestBilled == undefined) {
  //    this.selectedItem.posRestBilled = "0";
  //  } else {
  //    this.selectedItem.posRestBilled = this.frmProvProfAvgEdit.get('PosRestBilled').value.toString();
  //  }
  //  this.selectedItem.antRestBilled = this.frmProvProfAvgEdit.get('AntRestBilled').value
  //  if (this.selectedItem.antRestBilled == "" || this.selectedItem.antRestBilled == null || this.selectedItem.antRestBilled == undefined) {
  //    this.selectedItem.antRestBilled = "0";
  //  } else {
  //    this.selectedItem.antRestBilled = this.frmProvProfAvgEdit.get('AntRestBilled').value.toString();
  //  }
  //  this.selectedItem.restQtyPerMemb = this.frmProvProfAvgEdit.get('RestQtyPerMemb').value
  //  if (this.selectedItem.restQtyPerMemb == "" || this.selectedItem.restQtyPerMemb == null || this.selectedItem.restQtyPerMemb == undefined) {
  //    this.selectedItem.restQtyPerMemb = "0";
  //  } else {
  //    this.selectedItem.restQtyPerMemb = this.frmProvProfAvgEdit.get('RestQtyPerMemb').value.toString();
  //  }
  //  this.selectedItem.authToMemb = this.frmProvProfAvgEdit.get('AuthToMemb').value
  //  if (this.selectedItem.authToMemb == "" || this.selectedItem.authToMemb == null || this.selectedItem.authToMemb == undefined) {
  //    this.selectedItem.authToMemb = "0";
  //  } else {
  //    this.selectedItem.authToMemb = this.frmProvProfAvgEdit.get('AuthToMemb').value.toString();
  //  }
  //  this.selectedItem.claimCostNet = this.frmProvProfAvgEdit.get('ClaimCostNet').value
  //  if (this.selectedItem.claimCostNet == "" || this.selectedItem.claimCostNet == null || this.selectedItem.claimCostNet == undefined) {
  //    this.selectedItem.claimCostNet = "0";
  //  } else {
  //    this.selectedItem.claimCostNet = this.frmProvProfAvgEdit.get('ClaimCostNet').value.toString();
  //  }
  //  this.selectedItem.memberCost = this.frmProvProfAvgEdit.get('MemberCost').value
  //  if (this.selectedItem.memberCost == "" || this.selectedItem.memberCost == null || this.selectedItem.memberCost == undefined) {
  //    this.selectedItem.memberCost = "0";
  //  } else {
  //    this.selectedItem.memberCost = this.frmProvProfAvgEdit.get('MemberCost').value.toString();
  //  }
  //  this.selectedItem.visitrate = this.frmProvProfAvgEdit.get('Visitrate').value
  //  if (this.selectedItem.visitrate == "" || this.selectedItem.visitrate == null || this.selectedItem.visitrate == undefined) {
  //    this.selectedItem.visitrate = "0";
  //  } else {
  //    this.selectedItem.visitrate = this.frmProvProfAvgEdit.get('Visitrate').value.toString();
  //  }
  //  c.push(this.selectedItem);
    
  //  this.frmProvProfAvgEdit.reset();
  //  this._appService.CaptureService.UpdateProvProfAvg(c);
  //  this.showEdit = false;
  //  this.updateWarning = false;

  //}
}
