import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ProvProfDetail } from '../../models/ProvProfDetail';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-providerprofileddetails',
  templateUrl: './providerprofileddetails.component.html',
  styleUrls: ['./providerprofileddetails.component.css']
})
export class ProviderProfiledDetailsComponent implements OnInit {


  public showNew: boolean = false;
  public selectedItem: ProvProfDetail = new ProvProfDetail();
  public addNew: boolean = false;
  public _errMsg: ValidationErrors;
  public _errDate: any;
  public showEdit: boolean = false;
  public showDelete: boolean = false;
  public updateWarning: boolean = false;

  @Input() frmProvProfDetailNew: FormGroup;
  @Input() frmProvProfDetailEdit: FormGroup;


  constructor(public _appService: AppService, private fb: FormBuilder, private fbEdit: FormBuilder) {

    this._errMsg = {
      required: 'This field is required!',
      maxLength: 'Field Length Exceeded!',
      minLength: 'Field Length To Short!',
      familyNumber: 'Please complete a family number field first!',
      valid: 'valid',
      invalid: 'Invalid Format!'
    }

    this._appService.CaptureService.GetPRoviderProfileDetailData();

  }

  ngOnInit() {

    this.frmProvProfDetailNew = this.fb.group({

      'provid': ['', Validators.required],
      'AvgDate': [],
      'ProfileDt': ['', Validators.required],
      'firstAction': [],
      'reviewDate': [],
      'reviewAction': [],
      'provclass': [],
      'Pos1Billed': [],
      'Pos2Billed': [],
      'Pos3Billed': [],
      'Pos4Billed': [],
      'Ant1Billed': [],
      'Ant2Billed': [],
      'Ant3Billed': [],
      'Ant4Billed': [],
      'PosRestBilled': [],
      'AntRestBilled': [],
      'RestQtyPerMemb': [],
      'AuthToMemb': [],
      'ClaimCostNet': [],
      'MemberCost': [],
      'Visitrate': [],
      'comments': []

    });
    this.frmProvProfDetailEdit = this.fbEdit.group({

      'provid': ['', Validators.required],
      'AvgDate': [],
      'ProfileDt': ['', Validators.required],
      'firstAction': [],
      'reviewDate': [],
      'reviewAction': [],
      'provclass': [],
      'Pos1Billed': [],
      'Pos2Billed': [],
      'Pos3Billed': [],
      'Pos4Billed': [],
      'Ant1Billed': [],
      'Ant2Billed': [],
      'Ant3Billed': [],
      'Ant4Billed': [],
      'PosRestBilled': [],
      'AntRestBilled': [],
      'RestQtyPerMemb': [],
      'AuthToMemb': [],
      'ClaimCostNet': [],
      'MemberCost': [],
      'Visitrate': [],
       'comments': []
    });
  }

  //OpenModelNew() {
  //  this.showNew = true;
  //  this.frmProvProfDetailNew.reset()

  //}

  //ValidateDate() {
  //  let c: string = this.frmProvProfDetailNew.get('ProfileDt').value;

  //  if (c == "" || c == null || c == undefined) {
  //    this._errDate = this._errMsg['required'];
  //    this.frmProvProfDetailNew.controls['ProfileDt'].setErrors(this._errMsg['required']);
  //    this.frmProvProfDetailNew.controls['ProfileDt'].setValue('');
  //  } else {
  //    let e: string = c.replace("/", "");
  //    e = e.replace("/", "");

  //    this.frmProvProfDetailNew.controls['AvgDate'].setValue(e);
  //    let date: Date = new Date(parseInt(e.substr(0, 4)), parseInt(e.substr(4, 2)), parseInt(e.substr(6, 2)));
  //    let actionDt: Date = new Date(date.setMonth(date.getMonth() + 6))
  //    this.frmProvProfDetailNew.controls['reviewDate'].setValue(actionDt);
  //  }


  //}

  //getFormatedDate(date: Date, format: string) {
  //  const datePipe = new DatePipe('en-ZA');
  //  return datePipe.transform(date, format);
  //}

  //ShowNewWarning() {
  //  this.addNew = true;
  //}

  //CloseModelNEw() {

  //  this.showNew = false;
  //  this.selectedItem = new ProvProfDetail();
  //  this.addNew = false;
  //  this.frmProvProfDetailNew.reset();
  //}

  //AddNewData() {
  //  let c: ProvProfDetail = new ProvProfDetail();

  //  c.providerID = this.frmProvProfDetailNew.get('provid').value.toString();
  //  c.avgDate = this.frmProvProfDetailNew.get('AvgDate').value.toString();
  //  c.profileDt = this.frmProvProfDetailNew.get('ProfileDt').value.toString();
  //  c.firstAction = this.frmProvProfDetailNew.get('firstAction').value.toString();
  //  c.reviewDate = this.frmProvProfDetailNew.get('reviewDate').value.toString();
  //  c.reviewAction = this.frmProvProfDetailNew.get('reviewAction').value.toString();
  //  c.provclass = this.frmProvProfDetailNew.get('provclass').value.toString();

  //  c.pos1Billed = this.frmProvProfDetailNew.get('Pos1Billed').value
  //  if (c.pos1Billed == "" || c.pos1Billed == null || c.pos1Billed == undefined) {
  //    c.pos1Billed = "0";
  //  } else {
  //    c.pos1Billed = this.frmProvProfDetailNew.get('Pos1Billed').value.toString();
  //  }
  //  c.pos2Billed = this.frmProvProfDetailNew.get('Pos2Billed').value
  //  if (c.pos2Billed == "" || c.pos2Billed == null || c.pos2Billed == undefined) {
  //    c.pos2Billed = "0";
  //  } else {
  //    c.pos2Billed = this.frmProvProfDetailNew.get('Pos2Billed').value.toString();
  //  }
  //  c.pos3Billed = this.frmProvProfDetailNew.get('Pos3Billed').value
  //  if (c.pos3Billed == "" || c.pos3Billed == null || c.pos3Billed == undefined) {
  //    c.pos3Billed = "0";
  //  } else {
  //    c.pos3Billed = this.frmProvProfDetailNew.get('Pos3Billed').value.toString();
  //  }
  //  c.pos4Billed = this.frmProvProfDetailNew.get('Pos4Billed').value
  //  if (c.pos4Billed == "" || c.pos4Billed == null || c.pos4Billed == undefined) {
  //    c.pos4Billed = "0";
  //  } else {
  //    c.pos4Billed = this.frmProvProfDetailNew.get('Pos4Billed').value.toString();
  //  }
  //  c.ant1Billed = this.frmProvProfDetailNew.get('Ant1Billed').value
  //  if (c.ant1Billed == "" || c.ant1Billed == null || c.ant1Billed == undefined) {
  //    c.ant1Billed = "0";
  //  } else {
  //    c.ant1Billed = this.frmProvProfDetailNew.get('Ant1Billed').value.toString();
  //  }
  //  c.ant2Billed = this.frmProvProfDetailNew.get('Ant2Billed').value
  //  if (c.ant2Billed == "" || c.ant2Billed == null || c.ant2Billed == undefined) {
  //    c.ant2Billed = "0";
  //  } else {
  //    c.ant2Billed = this.frmProvProfDetailNew.get('Ant2Billed').value.toString();
  //  }
  //  c.ant3Billed = this.frmProvProfDetailNew.get('Ant3Billed').value
  //  if (c.ant3Billed == "" || c.ant3Billed == null || c.ant3Billed == undefined) {
  //    c.ant3Billed = "0";
  //  } else {
  //    c.ant3Billed = this.frmProvProfDetailNew.get('Ant3Billed').value.toString();
  //  }
  //  c.ant4Billed = this.frmProvProfDetailNew.get('Ant4Billed').value
  //  if (c.ant4Billed == "" || c.ant4Billed == null || c.ant4Billed == undefined) {
  //    c.ant4Billed = "0";
  //  } else {
  //    c.ant4Billed = this.frmProvProfDetailNew.get('Ant4Billed').value.toString();
  //  }
  //  c.posRestBilled = this.frmProvProfDetailNew.get('PosRestBilled').value
  //  if (c.posRestBilled == "" || c.posRestBilled == null || c.posRestBilled == undefined) {
  //    c.posRestBilled = "0";
  //  } else {
  //    c.posRestBilled = this.frmProvProfDetailNew.get('PosRestBilled').value.toString();
  //  }
  //  c.antRestBilled = this.frmProvProfDetailNew.get('AntRestBilled').value
  //  if (c.antRestBilled == "" || c.antRestBilled == null || c.antRestBilled == undefined) {
  //    c.antRestBilled = "0";
  //  } else {
  //    c.antRestBilled = this.frmProvProfDetailNew.get('AntRestBilled').value.toString();
  //  }
  //  c.restQtyPerMemb = this.frmProvProfDetailNew.get('RestQtyPerMemb').value
  //  if (c.restQtyPerMemb == "" || c.restQtyPerMemb == null || c.restQtyPerMemb == undefined) {
  //    c.restQtyPerMemb = "0";
  //  } else {
  //    c.restQtyPerMemb = this.frmProvProfDetailNew.get('RestQtyPerMemb').value.toString();
  //  }
  //  c.authToMemb = this.frmProvProfDetailNew.get('AuthToMemb').value
  //  if (c.authToMemb == "" || c.authToMemb == null || c.authToMemb == undefined) {
  //    c.authToMemb = "0";
  //  } else {
  //    c.authToMemb = this.frmProvProfDetailNew.get('AuthToMemb').value.toString();
  //  }
  //  c.claimCostNet = this.frmProvProfDetailNew.get('ClaimCostNet').value
  //  if (c.claimCostNet == "" || c.claimCostNet == null || c.claimCostNet == undefined) {
  //    c.claimCostNet = "0";
  //  } else {
  //    c.claimCostNet = this.frmProvProfDetailNew.get('ClaimCostNet').value.toString();
  //  }
  //  c.memberCost = this.frmProvProfDetailNew.get('MemberCost').value
  //  if (c.memberCost == "" || c.memberCost == null || c.memberCost == undefined) {
  //    c.memberCost = "0";
  //  } else {
  //    c.memberCost = this.frmProvProfDetailNew.get('MemberCost').value.toString();
  //  }
  //  c.visitrate = this.frmProvProfDetailNew.get('Visitrate').value
  //  if (c.visitrate == "" || c.visitrate == null || c.visitrate == undefined) {
  //    c.visitrate = "0";
  //  } else {
  //    c.visitrate = this.frmProvProfDetailNew.get('Visitrate').value.toString();
  //  }

  //  c.comments = this.frmProvProfDetailNew.get('comments').value
  //  if (c.comments == "" || c.comments == null || c.comments == undefined) {
  //    c.comments = "NO COMMENT";
  //  } else {
  //    c.comments = this.frmProvProfDetailNew.get('comments').value.toString();
  //  }
  //  this._appService.CaptureService.NewProvProfAvg(c);
  //  this.frmProvProfDetailNew.reset();
  //  this.showNew = false;
  //  this.addNew = false;

  //}

  //EditData(selected: ProvProfDetail) {
  //  this.selectedItem = new ProvProfDetail();
  //  this.selectedItem = selected;
  //  this.frmProvProfDetailEdit.controls['provid'].setValue(this.selectedItem.providerID);
  //  this.frmProvProfDetailEdit.controls['AvgDate'].setValue(this.selectedItem.avgDate);
  //  this.frmProvProfDetailEdit.controls['ProfileDt'].setValue(this.selectedItem.profileDt);
  //  this.frmProvProfDetailEdit.controls['firstAction'].setValue(this.selectedItem.firstAction);
  //  this.frmProvProfDetailEdit.controls['reviewDate'].setValue(this.selectedItem.reviewDate);
  //  this.frmProvProfDetailEdit.controls['reviewAction'].setValue(this.selectedItem.reviewAction);
  //  this.frmProvProfDetailEdit.controls['provclass'].setValue(this.selectedItem.provclass);
  //  this.frmProvProfDetailEdit.controls['Pos1Billed'].setValue(parseFloat(this.selectedItem.pos1Billed));
  //  this.frmProvProfDetailEdit.controls['Pos2Billed'].setValue(parseFloat(this.selectedItem.pos2Billed));
  //  this.frmProvProfDetailEdit.controls['Pos3Billed'].setValue(parseFloat(this.selectedItem.pos3Billed));
  //  this.frmProvProfDetailEdit.controls['Pos4Billed'].setValue(parseFloat(this.selectedItem.pos4Billed));
  //  this.frmProvProfDetailEdit.controls['Ant1Billed'].setValue(parseFloat(this.selectedItem.ant1Billed));
  //  this.frmProvProfDetailEdit.controls['Ant2Billed'].setValue(parseFloat(this.selectedItem.ant2Billed));
  //  this.frmProvProfDetailEdit.controls['Ant3Billed'].setValue(parseFloat(this.selectedItem.ant3Billed));
  //  this.frmProvProfDetailEdit.controls['Ant4Billed'].setValue(parseFloat(this.selectedItem.ant4Billed));
  //  this.frmProvProfDetailEdit.controls['PosRestBilled'].setValue(parseFloat(this.selectedItem.posRestBilled));
  //  this.frmProvProfDetailEdit.controls['AntRestBilled'].setValue(parseFloat(this.selectedItem.antRestBilled));
  //  this.frmProvProfDetailEdit.controls['RestQtyPerMemb'].setValue(parseFloat(this.selectedItem.restQtyPerMemb));
  //  this.frmProvProfDetailEdit.controls['AuthToMemb'].setValue(parseFloat(this.selectedItem.authToMemb));
  //  this.frmProvProfDetailEdit.controls['ClaimCostNet'].setValue(parseFloat(this.selectedItem.claimCostNet));
  //  this.frmProvProfDetailEdit.controls['MemberCost'].setValue(parseFloat(this.selectedItem.memberCost));
  //  this.frmProvProfDetailEdit.controls['Visitrate'].setValue(parseFloat(this.selectedItem.visitrate));
  //  this.frmProvProfDetailEdit.controls['comments'].setValue(this.selectedItem.comments);

  //  this.showEdit = true;



  //}

  //ShowUpdateWarning() {
  //  this.updateWarning = true;
  //}

  //ShowDeleteData(selected: ProvProfDetail) {
  //  this.selectedItem = new ProvProfDetail();

  //  this.selectedItem = selected;
  //  this.showDelete = true;

  //}

  //DeleteData() {
  //  this.showDelete = false;
  //  this._appService.CaptureService.DeleteProvProfAvg(this.selectedItem);

  //}

  //CloseModel() {
  //  this.showEdit = false;
  //  this.selectedItem = new ProvProfDetail();
  //}

  //UpdateData() {
  //  let originalData: ProvProfDetail = new ProvProfDetail();
  //  originalData = this.selectedItem;
  //  this.selectedItem = new ProvProfDetail();
  //  let c: ProvProfDetail[] = []

  //  c.push(originalData);
  //  this.selectedItem.providerID = this.frmProvProfDetailNew.get('provid').value.toString();
  //  this.selectedItem.avgDate = this.frmProvProfDetailEdit.get('AvgDate').value.toString();
  //  this.selectedItem.profileDt = this.frmProvProfDetailEdit.get('ProfileDt').value.toString();
  //  this.selectedItem.firstAction = this.frmProvProfDetailNew.get('firstAction').value.toString();
  //  this.selectedItem.reviewDate = this.frmProvProfDetailNew.get('reviewDate').value.toString();
  //  this.selectedItem.reviewAction = this.frmProvProfDetailNew.get('reviewAction').value.toString();
  //  this.selectedItem.provclass = this.frmProvProfDetailNew.get('provclass').value.toString();
  //  this.selectedItem.pos1Billed = this.frmProvProfDetailEdit.get('Pos1Billed').value

  //  if (this.selectedItem.pos1Billed == "" || this.selectedItem.pos1Billed == null || this.selectedItem.pos1Billed == undefined) {
  //    this.selectedItem.pos1Billed = "0";
  //  } else {
  //    this.selectedItem.pos1Billed = this.frmProvProfDetailEdit.get('Pos1Billed').value.toString();
  //  }
  //  this.selectedItem.pos2Billed = this.frmProvProfDetailEdit.get('Pos2Billed').value
  //  if (this.selectedItem.pos2Billed == "" || this.selectedItem.pos2Billed == null || this.selectedItem.pos2Billed == undefined) {
  //    this.selectedItem.pos2Billed = "0";
  //  } else {
  //    this.selectedItem.pos2Billed = this.frmProvProfDetailEdit.get('Pos2Billed').value.toString();
  //  }
  //  this.selectedItem.pos3Billed = this.frmProvProfDetailEdit.get('Pos3Billed').value
  //  if (this.selectedItem.pos3Billed == "" || this.selectedItem.pos3Billed == null || this.selectedItem.pos3Billed == undefined) {
  //    this.selectedItem.pos3Billed = "0";
  //  } else {
  //    this.selectedItem.pos3Billed = this.frmProvProfDetailEdit.get('Pos3Billed').value.toString();
  //  }
  //  this.selectedItem.pos4Billed = this.frmProvProfDetailEdit.get('Pos4Billed').value
  //  if (this.selectedItem.pos4Billed == "" || this.selectedItem.pos4Billed == null || this.selectedItem.pos4Billed == undefined) {
  //    this.selectedItem.pos4Billed = "0";
  //  } else {
  //    this.selectedItem.pos4Billed = this.frmProvProfDetailEdit.get('Pos4Billed').value.toString();
  //  }
  //  this.selectedItem.ant1Billed = this.frmProvProfDetailEdit.get('Ant1Billed').value
  //  if (this.selectedItem.ant1Billed == "" || this.selectedItem.ant1Billed == null || this.selectedItem.ant1Billed == undefined) {
  //    this.selectedItem.ant1Billed = "0";
  //  } else {
  //    this.selectedItem.ant1Billed = this.frmProvProfDetailEdit.get('Ant1Billed').value.toString();
  //  }
  //  this.selectedItem.ant2Billed = this.frmProvProfDetailEdit.get('Ant2Billed').value
  //  if (this.selectedItem.ant2Billed == "" || this.selectedItem.ant2Billed == null || this.selectedItem.ant2Billed == undefined) {
  //    this.selectedItem.ant2Billed = "0";
  //  } else {
  //    this.selectedItem.ant2Billed = this.frmProvProfDetailEdit.get('Ant2Billed').value.toString();
  //  }
  //  this.selectedItem.ant3Billed = this.frmProvProfDetailEdit.get('Ant3Billed').value
  //  if (this.selectedItem.ant3Billed == "" || this.selectedItem.ant3Billed == null || this.selectedItem.ant3Billed == undefined) {
  //    this.selectedItem.ant3Billed = "0";
  //  } else {
  //    this.selectedItem.ant3Billed = this.frmProvProfDetailEdit.get('Ant3Billed').value.toString();
  //  }
  //  this.selectedItem.ant4Billed = this.frmProvProfDetailEdit.get('Ant4Billed').value
  //  if (this.selectedItem.ant4Billed == "" || this.selectedItem.ant4Billed == null || this.selectedItem.ant4Billed == undefined) {
  //    this.selectedItem.ant4Billed = "0";
  //  } else {
  //    this.selectedItem.ant4Billed = this.frmProvProfDetailEdit.get('Ant4Billed').value.toString();
  //  }
  //  this.selectedItem.posRestBilled = this.frmProvProfDetailEdit.get('PosRestBilled').value
  //  if (this.selectedItem.posRestBilled == "" || this.selectedItem.posRestBilled == null || this.selectedItem.posRestBilled == undefined) {
  //    this.selectedItem.posRestBilled = "0";
  //  } else {
  //    this.selectedItem.posRestBilled = this.frmProvProfDetailEdit.get('PosRestBilled').value.toString();
  //  }
  //  this.selectedItem.antRestBilled = this.frmProvProfDetailEdit.get('AntRestBilled').value
  //  if (this.selectedItem.antRestBilled == "" || this.selectedItem.antRestBilled == null || this.selectedItem.antRestBilled == undefined) {
  //    this.selectedItem.antRestBilled = "0";
  //  } else {
  //    this.selectedItem.antRestBilled = this.frmProvProfDetailEdit.get('AntRestBilled').value.toString();
  //  }
  //  this.selectedItem.restQtyPerMemb = this.frmProvProfDetailEdit.get('RestQtyPerMemb').value
  //  if (this.selectedItem.restQtyPerMemb == "" || this.selectedItem.restQtyPerMemb == null || this.selectedItem.restQtyPerMemb == undefined) {
  //    this.selectedItem.restQtyPerMemb = "0";
  //  } else {
  //    this.selectedItem.restQtyPerMemb = this.frmProvProfDetailEdit.get('RestQtyPerMemb').value.toString();
  //  }
  //  this.selectedItem.authToMemb = this.frmProvProfDetailEdit.get('AuthToMemb').value
  //  if (this.selectedItem.authToMemb == "" || this.selectedItem.authToMemb == null || this.selectedItem.authToMemb == undefined) {
  //    this.selectedItem.authToMemb = "0";
  //  } else {
  //    this.selectedItem.authToMemb = this.frmProvProfDetailEdit.get('AuthToMemb').value.toString();
  //  }
  //  this.selectedItem.claimCostNet = this.frmProvProfDetailEdit.get('ClaimCostNet').value
  //  if (this.selectedItem.claimCostNet == "" || this.selectedItem.claimCostNet == null || this.selectedItem.claimCostNet == undefined) {
  //    this.selectedItem.claimCostNet = "0";
  //  } else {
  //    this.selectedItem.claimCostNet = this.frmProvProfDetailEdit.get('ClaimCostNet').value.toString();
  //  }
  //  this.selectedItem.memberCost = this.frmProvProfDetailEdit.get('MemberCost').value
  //  if (this.selectedItem.memberCost == "" || this.selectedItem.memberCost == null || this.selectedItem.memberCost == undefined) {
  //    this.selectedItem.memberCost = "0";
  //  } else {
  //    this.selectedItem.memberCost = this.frmProvProfDetailEdit.get('MemberCost').value.toString();
  //  }
  //  this.selectedItem.visitrate = this.frmProvProfDetailEdit.get('Visitrate').value
  //  if (this.selectedItem.visitrate == "" || this.selectedItem.visitrate == null || this.selectedItem.visitrate == undefined) {
  //    this.selectedItem.visitrate = "0";
  //  } else {
  //    this.selectedItem.visitrate = this.frmProvProfDetailEdit.get('Visitrate').value.toString();
  //  }
  //  this.selectedItem.comments = this.frmProvProfDetailNew.get('comments').value.toString();
  //  c.push(this.selectedItem);

  //  this.frmProvProfDetailEdit.reset();
  //  this._appService.CaptureService.UpdateProvProfAvg(c);
  //  this.showEdit = false;
  //  this.updateWarning = false;


  //}


  //ViewFiles(data: ProvProfDetail) {}

}
