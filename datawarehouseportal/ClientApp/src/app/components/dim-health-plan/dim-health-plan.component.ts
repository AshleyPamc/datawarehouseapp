import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { hpdata } from '../../models/hpdata';
import { HpDataNew } from '../../models/HpDataNew';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-dim-health-plan',
  templateUrl: './dim-health-plan.component.html',
  styleUrls: ['./dim-health-plan.component.css']
})
export class DimHealthPlanComponent implements OnInit {

  public updateWarning: boolean = false;
  public selectedItem: hpdata = new hpdata();
  public showEdit: boolean = false;
  public showDelete: boolean = false;
  public showNew: boolean = false;
  public addNew: boolean = false;
  @Input() frmHpData: FormGroup;
  @Input() frmHpDataNew: FormGroup;


  constructor(public _appService: AppService, private fb: FormBuilder, private fbNewData: FormBuilder) { }

  ngOnInit() {

    this.frmHpData = this.fb.group({
      'SpecialityGroup': [],
      'ServiceDateFrom': [],
      'ServiceDateTo': [],
      'FilterExcludeToDate': [],
      'FilterOnHP': [],
      'RevenueTypeHP': [],
      'WHERECLAUSE': [],
      'isRisk': [],
      'existFactClaim': []

    });

    this.frmHpDataNew = this.fb.group({
      'HpCode': [],
      'HpName': [],
      'LobCode': [],
      'HpLongDesc': [],
      'Opt': [],
      'HpType': [],
      'SpecialityGroup': [],
      'ServiceDateFrom': [],
      'ServiceDateTo': [],
      'FilterExcludeToDate': [],
      'FilterOnHP': [],
      'RevenueTypeHP': [],
      'WHERECLAUSE': [],
      'isRisk': [],
      'claimcount': [],
      'existFactClaim': []

    });
  }

  EditData(selected: hpdata) {
    this.selectedItem = new hpdata();
    this.selectedItem = selected;

    this.frmHpData.controls['SpecialityGroup'].setValue(selected.specialtyGrp);
    this.frmHpData.controls['ServiceDateFrom'].setValue(selected.svcDateFrom.substr(0, 10));
    this.frmHpData.controls['ServiceDateTo'].setValue(selected.svcDateTo.substr(0, 10));
    this.frmHpData.controls['FilterExcludeToDate'].setValue(selected.filterExcludeDt.substr(0, 10));

    if (selected.filterOnHp) {
      this.frmHpData.controls['FilterOnHP'].setValue('true');

    }
    else {
      this.frmHpData.controls['FilterOnHP'].setValue('false');

    }
    if (selected.existFactClaim) {
      this.frmHpData.controls['existFactClaim'].setValue('true');

    }
    else {
      this.frmHpData.controls['existFactClaim'].setValue('false');

    }
    this.frmHpData.controls['RevenueTypeHP'].setValue(selected.revenueTypeHp);
    this.frmHpData.controls['WHERECLAUSE'].setValue(selected.whereClause);
    if (selected.isRisk) {
      this.frmHpData.controls['isRisk'].setValue('true');

    }
    else {
      this.frmHpData.controls['isRisk'].setValue('false');

    }
    this.showEdit = true;

  }

  ShowDeleteData(selected: hpdata) {
    this.selectedItem = new hpdata();

    this.selectedItem = selected;
    this.showDelete = true;

  }

  DeleteData() {
    this.showDelete = false;
    this._appService.CaptureService.DeleteDimHealthplan(this.selectedItem);

  }



  OpenModelNew() {
    this.showNew = true;
    this.frmHpDataNew.reset()
  }

  CloseModelNew() {
    this.showNew = false;
    this.selectedItem = new hpdata();
    this.addNew = false;
    this.frmHpDataNew.reset();
  }

  ShowUpdateWarning() {
    this.updateWarning = true;
  }

  ShowNewWarning() {
    this.addNew = true;
  }
  UpdateData() {

    this.selectedItem.specialtyGrp = this.frmHpData.get('SpecialityGroup').value;
    this.selectedItem.svcDateFrom = this.frmHpData.get('ServiceDateFrom').value;
    this.selectedItem.svcDateTo = this.frmHpData.get('ServiceDateTo').value;
    this.selectedItem.filterExcludeDt = this.frmHpData.get('FilterExcludeToDate').value;
    if (this.frmHpData.get('existFactClaim').value == "false") {
      this.selectedItem.existFactClaim = false;
    } else {
      this.selectedItem.existFactClaim = true;
    }
    if (this.frmHpData.get('FilterOnHP').value == "false") {
      this.selectedItem.filterOnHp = false;
    } else {
      this.selectedItem.filterOnHp = true;
    }
    this.selectedItem.revenueTypeHp = this.frmHpData.get('RevenueTypeHP').value;
    this.selectedItem.whereClause = this.frmHpData.get('WHERECLAUSE').value;
    if (this.frmHpData.get('isRisk').value == "false") {
      this.selectedItem.isRisk = false;
    } else {
      this.selectedItem.isRisk = true;
    }
    this._appService.CaptureService.UpdateDimHealthplanData(this.selectedItem);
    this.showEdit = false;
    this.updateWarning = false;

  }
  CloseModel() {
    this.showEdit = false;
    this.selectedItem = new hpdata();
  }

  CloseModelNEw() {
    this.showNew = false;
    this.frmHpDataNew.reset();
  }
  AddNewData() {
    let c: HpDataNew = new HpDataNew();
 
    c.hpcode = this.frmHpDataNew.get('HpCode').value;
    c.hpname = this.frmHpDataNew.get('HpName').value;
    c.lobCode = this.frmHpDataNew.get('LobCode').value;
    c.hpLongDesc = this.frmHpDataNew.get('HpLongDesc').value;
    c.opt = this.frmHpDataNew.get('Opt').value;
    c.hpType = this.frmHpDataNew.get('HpType').value;
    c.revenueTypeHp = this.frmHpDataNew.get('RevenueTypeHP').value;
    c.svcDateFrom = this.frmHpDataNew.get('ServiceDateFrom').value;
    c.svcDateTo = this.frmHpDataNew.get('ServiceDateTo').value;
    c.whereClause = this.frmHpDataNew.get('WHERECLAUSE').value;
    c.filterExcludeDt = this.frmHpDataNew.get('FilterExcludeToDate').value;
    c.specialtyGrp = this.frmHpDataNew.get('SpecialityGroup').value;
    c.claimcount = this.frmHpDataNew.get('claimcount').value;
    if (this.frmHpDataNew.get('isRisk').value == "false") {
      c.isRisk = false;
    } else {
      c.isRisk = true;
    }
    if (this.frmHpDataNew.get('FilterOnHP').value == "false") {
     c.filterOnHp = false;
    } else {
      c.filterOnHp = true;
    }
    if (this.frmHpDataNew.get('existFactClaim').value == "false") {
      c.existFactClaim = false;
    } else {
      c.existFactClaim = true;
    }

    this._appService.CaptureService.NewDimHealthplanData(c);
    this.frmHpDataNew.reset();
    this.showNew = false;
    this.addNew = false;

  }

}
