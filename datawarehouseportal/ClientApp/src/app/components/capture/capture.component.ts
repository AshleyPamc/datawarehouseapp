import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { Revdata } from '../../models/revdata';
import {RevDataFiltering } from './rev-data-filtering';


@Component({
  selector: 'app-capture',
  templateUrl: './capture.component.html',
  styleUrls: ['./capture.component.css']
})
export class CaptureComponent implements OnInit{
  @Input() revenue: FormGroup;
  @Input() dateFilter: FormGroup;
  @Input() hpFilter: FormGroup;
  @Input() type: FormGroup;

  public _errMsg: ValidationErrors;
  public _errDate: any;
  public _errUnit: any;
  public _errBen: any;
  public _errRev: any;
  public _err8109: any;

  public _err8110: any;

  public _errVat: any;
  public update: boolean = false;
  public count_1: number = 0;

  public datafilter = new RevDataFiltering();

  constructor(public _appService: AppService, private fb: FormBuilder, private fbdate: FormBuilder, private fbhp: FormBuilder, private fbtype: FormBuilder) {
    this._errMsg = {
      required: 'This field is required!',
      maxLength: 'Field Length Exceeded!',
      minLength: 'Field Length To Short!',
      familyNumber: 'Please complete a family number field first!',
      valid: 'valid',
      invalid: 'Invalid Format!'
    };

  }

  ngOnInit() {
    this.revenue = this.fb.group({
      date: ['', Validators.required],
      datesid: ['', Validators.required],
      hpOptions: ['', Validators.required],
      bencount: ['', Validators.required],
      unit: ['', Validators.required],
      opt: ['', Validators.required],
      type: ['', Validators.required],
      rev: ['', Validators.required],
      vat: ['', Validators.required],
      amount8110: [''],
      amount8109: [''],
      adjRev: ['', Validators.required]
    });
    this.revenue.get('datesid').disable();

    this.dateFilter = this.fbdate.group({
      datefil: []
    });
    this.hpFilter = this.fbhp.group({
      hp: [],
      options: [],
    });

    this.type = this.fbtype.group({
      type: [],
    });

  }

  ValidateDate() {
    let c = this.revenue.get('date').value;
    if ((c == '') || (c == undefined) || (c == null)) {
      this._errDate = this._errMsg['required'];
      this.revenue.controls['date'].setErrors(this._errMsg['required']);
      this.revenue.controls['datesid'].setValue('');
    } else {
      let d = new Date(c)
      let h = new Date;
      if (d > h) {
        this._errDate = 'Chosen date cannot be greater than ' + h.toLocaleString();
        this.revenue.controls['date'].setErrors(this._errMsg['invalid']);
        this.revenue.controls['datesid'].setValue('');
      } else {
        this._errDate = '';
        let id: string ='';
        let temp: string = d.toLocaleString().substr(0, 10);;
        id = temp.substr(6, 4) + temp.substr(3, 2) + temp.substr(0, 2) ;
        this.revenue.controls['datesid'].setValue(id);
      }
    }

  }

  ValidateUnit() {
    let c = this.revenue.get('unit').value;
    let d = this.revenue.get('bencount').value;
    if ((c == '') || (c == undefined) || (c == null)) {
      this._errUnit = this._errMsg['required'];
      this.revenue.controls['unit'].setErrors(this._errMsg['required']);
      this.revenue.controls['unit'].setValue('');
    }
    if ((d == '') || (d == undefined) || (d == null)) {
      this._errBen = this._errMsg['required'];
      this.revenue.controls['bencount'].setErrors(this._errMsg['required']);
      this.revenue.controls['bencount'].setValue('');
    } else {
      let e = parseFloat(c) * parseFloat(d);
      this.revenue.controls['rev'].setValue(e.toFixed(2));
      this.ValidateRev();
    }

  }

  ValidateRev() {
    let c = this.revenue.get('rev').value;
    if ((c == '') || (c == undefined) || (c == null)) {
      this._errRev = this._errMsg['required'];
      this.revenue.controls['rev'].setErrors(this._errMsg['required']);
      this.revenue.controls['rev'].setValue('');
    } else {
      let d: number = 0;
      d = c * (15 / 115);
      this.revenue.controls['vat'].setValue(d.toFixed(2));
      this.ValidateVat8109();
    }
  }

  ValidateVat8109() {
    let c = this.revenue.get('amount8109').value;
    let d = this.revenue.get('amount8110').value;

    if ((c == '') || (c == undefined) || (c == null)) {

     
      this.revenue.controls['amount8109'].setValue('0.00');
      c = '0.00';
    }
    if ((d == '') || (d == undefined) || (d == null)) {
      this._err8110 = this._errMsg['required'];
      this.revenue.controls['amount8110'].setValue('0.00');
      d = '0.00';
    } //else {
      let tax = this.revenue.get('vat').value;
      let rev = this.revenue.get('rev').value;
      let normalRev = parseFloat(rev) - parseFloat(tax);
      let totalIncVat = ((normalRev + parseFloat(c) + parseFloat(d))) + ((normalRev + parseFloat(c) + parseFloat(d)) * (15 / 115));
      let totalExcludeVat = normalRev + parseFloat(c) + parseFloat(d)
      this.revenue.controls['adjRev'].setValue(totalExcludeVat.toFixed(2));



    //}
  }

  Sort() {
    if (this.count_1 == 0) {
      this.count_1++;
      this._appService.CaptureService._data = this._appService.CaptureService._data.sort((a, b) => a.invoiceDate > b.invoiceDate ? -1 : 1);
    } else {
      if (this.count_1 == 1) {
        this.count_1 = 0;
        this._appService.CaptureService._data = this._appService.CaptureService._data.sort((a, b) => b.invoiceDate >a.invoiceDate   ? -1 : 1);
      }
    }

  }

  ValidateVat() {
    let c = this.revenue.get('vat').value;
    if ((c == '') || (c == undefined) || (c == null)) {
      this._errVat = this._errMsg['required'];
      this.revenue.controls['vat'].setErrors(this._errMsg['required']);
      this.revenue.controls['vat'].setValue('');
    }
  }

  ValidateBenCount() {
    let c = this.revenue.get('bencount').value;
    if ((c == '') || (c == undefined) || (c == null)) {
      this._errBen = this._errMsg['required'];
      this.revenue.controls['bencount'].setErrors(this._errMsg['required']);
      this.revenue.controls['bencount'].setValue('');
    }
  }

  GetOptsFilter() {
    let hpp = this.hpFilter.get('hp').value;
    this._appService.CaptureService._options = [];
    this._appService.CaptureService._options = this._appService.CaptureService.hpplans.filter((a) => {
      if ((a.hpname == hpp)) {
        return a;
      }
    });
  }
  GetOpts() {
    let hp = this.revenue.get('hpOptions').value;
    this._appService.CaptureService._options = [];
    this._appService.CaptureService._options = this._appService.CaptureService.hpplans.filter((a) => {
      if ((a.hpname == hp)) {
        return a;
      }
    });
  }

  OpenModel() {
    this.revenue.reset();
    this.update = false;
    this._appService.CaptureService.revenueModal = true;
  }

  EditData(selected: Revdata) {

    this._appService.CaptureService._updateRevData = [];
    this._appService.CaptureService._updateRevData.push(selected);
    this.revenue.reset();
    this.revenue.controls['date'].setValue(selected.invoiceDate.replace('-','/'));
    this.revenue.controls['bencount'].setValue(selected.bencount);
    this.revenue.controls['unit'].setValue(selected.unitprice);
    this.revenue.controls['type'].setValue(selected.typeOpt);
    this.revenue.controls['rev'].setValue(selected.incVat);
    this.revenue.controls['vat'].setValue(selected.vat);
    this.revenue.controls['amount8109'].setValue(selected.amount8109);
    this.revenue.controls['amount8110'].setValue(selected.amount8110);
    this.revenue.controls['adjRev'].setValue(selected.adjAmountExecVat);



    let c: string[] = [];
    c = selected.hpname.split(' - ');
    this._appService.CaptureService._hp.forEach((el) => {
      if (el.hpname == c[0]) {
        this.revenue.controls['hpOptions'].setValue(el.hpname);
      }
    });

    this.GetOpts();

    this._appService.CaptureService._options.forEach((el) => {
      if (el.sid == parseInt( selected.hpsid)) {
        this.revenue.controls['opt'].setValue(el.sid);
      }
    });

    this.update = true;
    this._appService.CaptureService.revenueModal = true;

  }

  UpdateData() {
    this._appService.CaptureService._newRevData = new Revdata();
    this._appService.CaptureService._newRevData.invoiceDate = this.revenue.get('date').value;
    this.ValidateDate();
    this._appService.CaptureService._newRevData.invoicesid = this.revenue.get('datesid').value;
    this._appService.CaptureService._newRevData.hpsid = this.revenue.get('opt').value.toString();
    this._appService.CaptureService._newRevData.bencount = this.revenue.get('bencount').value.toString();
    this._appService.CaptureService._newRevData.unitprice = parseFloat(this.revenue.get('unit').value).toFixed(2);
    this._appService.CaptureService._newRevData.incVat = this.revenue.get('rev').value;
    this._appService.CaptureService._newRevData.vat = this.revenue.get('vat').value;
    this._appService.CaptureService._newRevData.excVat = (parseFloat(this.revenue.get('rev').value) - parseFloat(this.revenue.get('vat').value)).toFixed(2);
    this._appService.CaptureService._newRevData.typeOpt = this.revenue.get('type').value;
    if (this.revenue.get('amount8109').value.toString() == "0" || this.revenue.get('amount8109').value.toString() == "0.00" || this.revenue.get('amount8109').value == null
      || this.revenue.get('amount8109').value == undefined) {
      this._appService.CaptureService._newRevData.amount8109 = "0.00";
    } else {
      this._appService.CaptureService._newRevData.amount8109 = this.revenue.get('amount8109').value.toFixed(2);
    }
    if (this.revenue.get('amount8110').value.toString() == "0" || this.revenue.get('amount8110').value.toString() == "0.00" || this.revenue.get('amount8110').value == null
      || this.revenue.get('amount8110').value == undefined) {
      this._appService.CaptureService._newRevData.amount8110 = "0.00";
    } else {
      this._appService.CaptureService._newRevData.amount8110 = this.revenue.get('amount8110').value.toFixed(2);
    }
   
    this._appService.CaptureService._newRevData.adjAmountExecVat = this.revenue.get('adjRev').value;
    this._appService.CaptureService._newRevData.adjAmount = parseFloat((parseFloat(this.revenue.get('adjRev').value) + (parseFloat(this.revenue.get('adjRev').value) * (15 / 115))).toFixed(2)).toFixed(2);

    this._appService.CaptureService._update = true;
  }

  AddNewData() {
    this._appService.CaptureService._newRevData = new Revdata();
    this._appService.CaptureService._newRevData.invoiceDate = this.revenue.get('date').value;
    this._appService.CaptureService._newRevData.invoicesid = this.revenue.get('datesid').value;
    this._appService.CaptureService._newRevData.hpsid = this.revenue.get('opt').value.toString();
    this._appService.CaptureService._newRevData.bencount = this.revenue.get('bencount').value.toString();
    this._appService.CaptureService._newRevData.unitprice = parseFloat(this.revenue.get('unit').value).toFixed(2);
    this._appService.CaptureService._newRevData.incVat = parseFloat(this.revenue.get('rev').value).toFixed(2);
    this._appService.CaptureService._newRevData.vat = this.revenue.get('vat').value;
    this._appService.CaptureService._newRevData.excVat = (parseFloat(this.revenue.get('rev').value) - parseFloat(this.revenue.get('vat').value)).toFixed(2);
    this._appService.CaptureService._newRevData.typeOpt = this.revenue.get('type').value;
    if (this.revenue.get('amount8109').value.toString() == "0" || this.revenue.get('amount8109').value.toString() == "0.00" || this.revenue.get('amount8109').value == null
      || this.revenue.get('amount8109').value == undefined) {
      this._appService.CaptureService._newRevData.amount8109 = "0.00";
    } else {
      this._appService.CaptureService._newRevData.amount8109 = this.revenue.get('amount8109').value.toFixed(2);
    }
    if (this.revenue.get('amount8110').value.toString() == "0" || this.revenue.get('amount8110').value.toString() == "0.00" || this.revenue.get('amount8110').value == null
      || this.revenue.get('amount8110').value == undefined) {
      this._appService.CaptureService._newRevData.amount8110 = "0.00";
    } else {
      this._appService.CaptureService._newRevData.amount8110 = this.revenue.get('amount8110').value.toFixed(2);
    }
    this._appService.CaptureService._newRevData.adjAmountExecVat = this.revenue.get('adjRev').value;
    this._appService.CaptureService._newRevData.adjAmount = parseFloat((parseFloat(this.revenue.get('adjRev').value) + (parseFloat(this.revenue.get('adjRev').value) * (15 / 115))).toFixed(2)).toFixed(2);

    this._appService.CaptureService._add = true;
    



  }

  CloseModel() {
    this.revenue.reset();
    this._appService.CaptureService.revenueModal = false;
  }

  DeleteSelected(data: Revdata) {
    this._appService.CaptureService._deleteRevData = new Revdata();
    this._appService.CaptureService._deleteRevData = data;
    this._appService.CaptureService._delete = true;
  }

  OpenDateFilter() {
    this.dateFilter.reset();
    this._appService.CaptureService._filterDate = true;
  }

  OpenHpOptFilter() {
    this.hpFilter.reset();
    this._appService.CaptureService._hpOpt = true;
  }

  OpenTypeFilter() {
    this.type.reset();
    this._appService.CaptureService._type = true;
  }

  ApplyDate() {
    let c = this._appService.CaptureService._data;
    let date = this.dateFilter.get('datefil').value;

    this._appService.CaptureService._data = [];

    c.forEach((el) => {
      if (el.invoiceDate.trim() == date) {
        this._appService.CaptureService._data.push(el);
      }
    })
    this._appService.CaptureService._filterDate = false;
  }

  RemoveFilter() {
    this._appService.CaptureService.GetRevData();
    this._appService.CaptureService._filterDate = false;
    this._appService.CaptureService._hpOpt = false;
    this._appService.CaptureService._type = false;
  }

  ApplyHpOpt() {
    let c = this.hpFilter.get('hp').value;
    let d = this.hpFilter.get('options').value;

    this._appService.CaptureService._data = this._appService.CaptureService._data.filter(
      (a) => a.hpname == c + " - " + d)

    this._appService.CaptureService._hpOpt = false;

  }

  public count: number = 0;
  SortBenCount() {
    if (this.count == 0) {
      this._appService.CaptureService._data = this._appService.CaptureService._data.sort((a, b) => a.bencount > b.bencount ? -1 : 1);
      this.count++;
    } else {
      this._appService.CaptureService._data = this._appService.CaptureService._data.sort((a, b) => a.bencount < b.bencount ? -1 : 1);
      this.count = 0;
    }

  }

  public countrev: number = 0;
  SortRevVat() {
    if (this.countrev == 0) {
      this._appService.CaptureService._data = this._appService.CaptureService._data.sort((a, b) => a.incVat > b.incVat ? -1 : 1);
      this.countrev++;
    } else {
      this._appService.CaptureService._data = this._appService.CaptureService._data.sort((a, b) => a.incVat < b.incVat ? -1 : 1);
      this.countrev = 0;
    }
  }

  public countUnit: number = 0;
  SortUnit() {
    if (this.countUnit == 0) {
      this._appService.CaptureService._data = this._appService.CaptureService._data.sort((a, b) => a.unitprice > b.unitprice ? -1 : 1);
      this.countUnit++;
    } else {
      this._appService.CaptureService._data = this._appService.CaptureService._data.sort((a, b) => a.unitprice < b.unitprice ? -1 : 1);
      this.countUnit = 0;
    }
  }

  ApplyType() {
    let c = this.type.get('type').value;

    this._appService.CaptureService._data = this._appService.CaptureService._data.filter(
      (a) => a.typeOpt == c)

    this._appService.CaptureService._type = false;
  }

}
