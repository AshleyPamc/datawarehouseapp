
import { Revdata } from './../../models/revdata'
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class RevDataFiltering implements ClrDatagridStringFilterInterface<Revdata> {
  accepts(field: Revdata, search: string): boolean {
    return "" + field.invoiceDate == search;

  }
}
